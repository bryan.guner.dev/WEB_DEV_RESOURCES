import speedypup from '../../pups/speedy-pup.jpg';

const PupImage = () => {
  return <img src={speedypup} alt='pup' />;
};

export default PupImage;
