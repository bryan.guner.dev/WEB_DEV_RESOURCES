# All commands sorted by votes

> A repository for the most elegant and useful UNIX commands.  Great commands can be shared, discussed and voted on to provide a comprehensive resource for working from the command-line

### What's this?

**commandlinefu.com** is the place to record those command-line gems that you return to again and again. That way others can gain from your CLI wisdom and you from theirs too. All commands can be commented on, discussed and voted up or down.

[**Share Your Commands**](https://www.commandlinefu.com/commands/edit)

### Check These Out

Works for repos cloned via ssh or https.

swap out "80" for your port of interest. Can use port number or named ports e.g. "http"

this string of commands will release your dhcp address, change your mac address, generate a new random hostname and then get a new dhcp lease.

swap out "80" for your port of interest. Can use port number or named ports e.g. "http"

Replace 'csv\_file.csv' with your filename.

Put this in your zshrc, source it, then run 'pkill -usr1 zsh' to source it in all open terminals. Also works with bash. More info: http://www.reddit.com/r/commandline/comments/12g76v/how\_to\_automatically\_source\_zshrc\_in\_all\_open/

Draw a telephone keyboard, using just a shell built-in command.

$ secret\_command;export HISTCONTROL= This will make "secret\_command" not appear in "history" list.


[Source](https://www.commandlinefu.com/commands/browse/sort-by-votes)
