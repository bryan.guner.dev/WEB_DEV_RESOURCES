<h1 id="raw-queries">Raw Queries</h1>
<p>As there are often use cases in which it is just easier to execute raw / already prepared SQL queries, you can use the <a href="../class/lib/sequelize.js~Sequelize.html#instance-method-query"><code>sequelize.query</code></a> method.</p>
<p>By default the function will return two arguments - a results array, and an object containing metadata (such as amount of affected rows, etc). Note that since this is a raw query, the metadata are dialect specific. Some dialects return the metadata “within” the results object (as properties on an array). However, two arguments will always be returned, but for MSSQL and MySQL it will be two references to the same object.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb1-1" title="1"><span class="kw">const</span> [results<span class="op">,</span> metadata] <span class="op">=</span> <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(<span class="st">&quot;UPDATE users SET y = 42 WHERE x = 12&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb1-2" title="2"><span class="co">// Results will be an empty array and metadata will contain the number of affected rows.</span></a></code></pre></div>
<p>In cases where you don’t need to access the metadata you can pass in a query type to tell sequelize how to format the results. For example, for a simple select query you could do:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb2-1" title="1"><span class="kw">const</span> <span class="op">{</span> QueryTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb2-2" title="2"><span class="kw">const</span> users <span class="op">=</span> <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(<span class="st">&quot;SELECT * FROM `users`&quot;</span><span class="op">,</span> <span class="op">{</span> <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span> <span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb2-3" title="3"><span class="co">// We didn&#39;t need to destructure the result here - the results were returned directly</span></a></code></pre></div>
<p>Several other query types are available. <a href="https://github.com/sequelize/sequelize/blob/master/src/query-types.ts">Peek into the source for details</a>.</p>
<p>A second option is the model. If you pass a model the returned data will be instances of that model.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb3-1" title="1"><span class="co">// Callee is the model definition. This allows you to easily map a query to a predefined model</span></a>
<a class="sourceLine" id="cb3-2" title="2"><span class="kw">const</span> projects <span class="op">=</span> <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(<span class="st">&#39;SELECT * FROM projects&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb3-3" title="3">  <span class="dt">model</span><span class="op">:</span> Projects<span class="op">,</span></a>
<a class="sourceLine" id="cb3-4" title="4">  <span class="dt">mapToModel</span><span class="op">:</span> <span class="kw">true</span> <span class="co">// pass true here if you have any mapped fields</span></a>
<a class="sourceLine" id="cb3-5" title="5"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb3-6" title="6"><span class="co">// Each element of `projects` is now an instance of Project</span></a></code></pre></div>
<p>See more options in the <a href="../class/lib/sequelize.js~Sequelize.html#instance-method-query">query API reference</a>. Some examples:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb4-1" title="1"><span class="kw">const</span> <span class="op">{</span> QueryTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-2" title="2"><span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(<span class="st">&#39;SELECT 1&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb4-3" title="3">  <span class="co">// A function (or false) for logging your queries</span></a>
<a class="sourceLine" id="cb4-4" title="4">  <span class="co">// Will get called for every SQL query that gets sent</span></a>
<a class="sourceLine" id="cb4-5" title="5">  <span class="co">// to the server.</span></a>
<a class="sourceLine" id="cb4-6" title="6">  <span class="dt">logging</span><span class="op">:</span> <span class="va">console</span>.<span class="at">log</span><span class="op">,</span></a>
<a class="sourceLine" id="cb4-7" title="7"></a>
<a class="sourceLine" id="cb4-8" title="8">  <span class="co">// If plain is true, then sequelize will only return the first</span></a>
<a class="sourceLine" id="cb4-9" title="9">  <span class="co">// record of the result set. In case of false it will return all records.</span></a>
<a class="sourceLine" id="cb4-10" title="10">  <span class="dt">plain</span><span class="op">:</span> <span class="kw">false</span><span class="op">,</span></a>
<a class="sourceLine" id="cb4-11" title="11"></a>
<a class="sourceLine" id="cb4-12" title="12">  <span class="co">// Set this to true if you don&#39;t have a model definition for your query.</span></a>
<a class="sourceLine" id="cb4-13" title="13">  <span class="dt">raw</span><span class="op">:</span> <span class="kw">false</span><span class="op">,</span></a>
<a class="sourceLine" id="cb4-14" title="14"></a>
<a class="sourceLine" id="cb4-15" title="15">  <span class="co">// The type of query you are executing. The query type affects how results are formatted before they are passed back.</span></a>
<a class="sourceLine" id="cb4-16" title="16">  <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb4-17" title="17"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-18" title="18"></a>
<a class="sourceLine" id="cb4-19" title="19"><span class="co">// Note the second argument being null!</span></a>
<a class="sourceLine" id="cb4-20" title="20"><span class="co">// Even if we declared a callee here, the raw: true would</span></a>
<a class="sourceLine" id="cb4-21" title="21"><span class="co">// supersede and return a raw object.</span></a>
<a class="sourceLine" id="cb4-22" title="22"><span class="va">console</span>.<span class="at">log</span>(<span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(<span class="st">&#39;SELECT * FROM projects&#39;</span><span class="op">,</span> <span class="op">{</span> <span class="dt">raw</span><span class="op">:</span> <span class="kw">true</span> <span class="op">}</span>))<span class="op">;</span></a></code></pre></div>
<h2 id="dotted-attributes-and-the-nest-option">“Dotted” attributes and the <code>nest</code> option</h2>
<p>If an attribute name of the table contains dots, the resulting objects can become nested objects by setting the <code>nest: true</code> option. This is achieved with <a href="https://github.com/mickhansen/dottie.js/">dottie.js</a> under the hood. See below:</p>
<ul>
<li><p>Without <code>nest: true</code>:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb5-1" title="1"><span class="kw">const</span> <span class="op">{</span> QueryTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb5-2" title="2"><span class="kw">const</span> records <span class="op">=</span> <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(<span class="st">&#39;select 1 as `foo.bar.baz`&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb5-3" title="3">  <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb5-4" title="4"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb5-5" title="5"><span class="va">console</span>.<span class="at">log</span>(<span class="va">JSON</span>.<span class="at">stringify</span>(records[<span class="dv">0</span>]<span class="op">,</span> <span class="kw">null</span><span class="op">,</span> <span class="dv">2</span>))<span class="op">;</span></a></code></pre></div>
<div class="sourceCode" id="cb6"><pre class="sourceCode json"><code class="sourceCode json"><a class="sourceLine" id="cb6-1" title="1"><span class="fu">{</span></a>
<a class="sourceLine" id="cb6-2" title="2">  <span class="dt">&quot;foo.bar.baz&quot;</span><span class="fu">:</span> <span class="dv">1</span></a>
<a class="sourceLine" id="cb6-3" title="3"><span class="fu">}</span></a></code></pre></div></li>
<li><p>With <code>nest: true</code>:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb7-1" title="1"><span class="kw">const</span> <span class="op">{</span> QueryTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb7-2" title="2"><span class="kw">const</span> records <span class="op">=</span> <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(<span class="st">&#39;select 1 as `foo.bar.baz`&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb7-3" title="3">  <span class="dt">nest</span><span class="op">:</span> <span class="kw">true</span><span class="op">,</span></a>
<a class="sourceLine" id="cb7-4" title="4">  <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb7-5" title="5"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb7-6" title="6"><span class="va">console</span>.<span class="at">log</span>(<span class="va">JSON</span>.<span class="at">stringify</span>(records[<span class="dv">0</span>]<span class="op">,</span> <span class="kw">null</span><span class="op">,</span> <span class="dv">2</span>))<span class="op">;</span></a></code></pre></div>
<div class="sourceCode" id="cb8"><pre class="sourceCode json"><code class="sourceCode json"><a class="sourceLine" id="cb8-1" title="1"><span class="fu">{</span></a>
<a class="sourceLine" id="cb8-2" title="2">  <span class="dt">&quot;foo&quot;</span><span class="fu">:</span> <span class="fu">{</span></a>
<a class="sourceLine" id="cb8-3" title="3">    <span class="dt">&quot;bar&quot;</span><span class="fu">:</span> <span class="fu">{</span></a>
<a class="sourceLine" id="cb8-4" title="4">      <span class="dt">&quot;baz&quot;</span><span class="fu">:</span> <span class="dv">1</span></a>
<a class="sourceLine" id="cb8-5" title="5">    <span class="fu">}</span></a>
<a class="sourceLine" id="cb8-6" title="6">  <span class="fu">}</span></a>
<a class="sourceLine" id="cb8-7" title="7"><span class="fu">}</span></a></code></pre></div></li>
</ul>
<h2 id="replacements">Replacements</h2>
<p>Replacements in a query can be done in two different ways, either using named parameters (starting with <code>:</code>), or unnamed, represented by a <code>?</code>. Replacements are passed in the options object.</p>
<ul>
<li>If an array is passed, <code>?</code> will be replaced in the order that they appear in the array</li>
<li>If an object is passed, <code>:key</code> will be replaced with the keys from that object. If the object contains keys not found in the query or vice versa, an exception will be thrown.</li>
</ul>
<div class="sourceCode" id="cb9"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb9-1" title="1"><span class="kw">const</span> <span class="op">{</span> QueryTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb9-2" title="2"></a>
<a class="sourceLine" id="cb9-3" title="3"><span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(</a>
<a class="sourceLine" id="cb9-4" title="4">  <span class="st">&#39;SELECT * FROM projects WHERE status = ?&#39;</span><span class="op">,</span></a>
<a class="sourceLine" id="cb9-5" title="5">  <span class="op">{</span></a>
<a class="sourceLine" id="cb9-6" title="6">    <span class="dt">replacements</span><span class="op">:</span> [<span class="st">&#39;active&#39;</span>]<span class="op">,</span></a>
<a class="sourceLine" id="cb9-7" title="7">    <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb9-8" title="8">  <span class="op">}</span></a>
<a class="sourceLine" id="cb9-9" title="9">)<span class="op">;</span></a>
<a class="sourceLine" id="cb9-10" title="10"></a>
<a class="sourceLine" id="cb9-11" title="11"><span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(</a>
<a class="sourceLine" id="cb9-12" title="12">  <span class="st">&#39;SELECT * FROM projects WHERE status = :status&#39;</span><span class="op">,</span></a>
<a class="sourceLine" id="cb9-13" title="13">  <span class="op">{</span></a>
<a class="sourceLine" id="cb9-14" title="14">    <span class="dt">replacements</span><span class="op">:</span> <span class="op">{</span> <span class="dt">status</span><span class="op">:</span> <span class="st">&#39;active&#39;</span> <span class="op">},</span></a>
<a class="sourceLine" id="cb9-15" title="15">    <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb9-16" title="16">  <span class="op">}</span></a>
<a class="sourceLine" id="cb9-17" title="17">)<span class="op">;</span></a></code></pre></div>
<p>Array replacements will automatically be handled, the following query searches for projects where the status matches an array of values.</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb10-1" title="1"><span class="kw">const</span> <span class="op">{</span> QueryTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-2" title="2"></a>
<a class="sourceLine" id="cb10-3" title="3"><span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(</a>
<a class="sourceLine" id="cb10-4" title="4">  <span class="st">&#39;SELECT * FROM projects WHERE status IN(:status)&#39;</span><span class="op">,</span></a>
<a class="sourceLine" id="cb10-5" title="5">  <span class="op">{</span></a>
<a class="sourceLine" id="cb10-6" title="6">    <span class="dt">replacements</span><span class="op">:</span> <span class="op">{</span> <span class="dt">status</span><span class="op">:</span> [<span class="st">&#39;active&#39;</span><span class="op">,</span> <span class="st">&#39;inactive&#39;</span>] <span class="op">},</span></a>
<a class="sourceLine" id="cb10-7" title="7">    <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb10-8" title="8">  <span class="op">}</span></a>
<a class="sourceLine" id="cb10-9" title="9">)<span class="op">;</span></a></code></pre></div>
<p>To use the wildcard operator <code>%</code>, append it to your replacement. The following query matches users with names that start with ‘ben’.</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb11-1" title="1"><span class="kw">const</span> <span class="op">{</span> QueryTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb11-2" title="2"></a>
<a class="sourceLine" id="cb11-3" title="3"><span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(</a>
<a class="sourceLine" id="cb11-4" title="4">  <span class="st">&#39;SELECT * FROM users WHERE name LIKE :search_name&#39;</span><span class="op">,</span></a>
<a class="sourceLine" id="cb11-5" title="5">  <span class="op">{</span></a>
<a class="sourceLine" id="cb11-6" title="6">    <span class="dt">replacements</span><span class="op">:</span> <span class="op">{</span> <span class="dt">search_name</span><span class="op">:</span> <span class="st">&#39;ben%&#39;</span> <span class="op">},</span></a>
<a class="sourceLine" id="cb11-7" title="7">    <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb11-8" title="8">  <span class="op">}</span></a>
<a class="sourceLine" id="cb11-9" title="9">)<span class="op">;</span></a></code></pre></div>
<h2 id="bind-parameter">Bind Parameter</h2>
<p>Bind parameters are like replacements. Except replacements are escaped and inserted into the query by sequelize before the query is sent to the database, while bind parameters are sent to the database outside the SQL query text. A query can have either bind parameters or replacements. Bind parameters are referred to by either $1, $2, … (numeric) or $key (alpha-numeric). This is independent of the dialect.</p>
<ul>
<li>If an array is passed, <code>$1</code> is bound to the 1st element in the array (<code>bind[0]</code>)</li>
<li>If an object is passed, <code>$key</code> is bound to <code>object['key']</code>. Each key must begin with a non-numeric char. <code>$1</code> is not a valid key, even if <code>object['1']</code> exists.</li>
<li>In either case <code>$$</code> can be used to escape a literal <code>$</code> sign.</li>
</ul>
<p>The array or object must contain all bound values or Sequelize will throw an exception. This applies even to cases in which the database may ignore the bound parameter.</p>
<p>The database may add further restrictions to this. Bind parameters cannot be SQL keywords, nor table or column names. They are also ignored in quoted text or data. In PostgreSQL it may also be needed to typecast them, if the type cannot be inferred from the context <code>$1::varchar</code>.</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb12-1" title="1"><span class="kw">const</span> <span class="op">{</span> QueryTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb12-2" title="2"></a>
<a class="sourceLine" id="cb12-3" title="3"><span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(</a>
<a class="sourceLine" id="cb12-4" title="4">  <span class="st">&#39;SELECT *, &quot;text with literal $$1 and literal $$status&quot; as t FROM projects WHERE status = $1&#39;</span><span class="op">,</span></a>
<a class="sourceLine" id="cb12-5" title="5">  <span class="op">{</span></a>
<a class="sourceLine" id="cb12-6" title="6">    <span class="dt">bind</span><span class="op">:</span> [<span class="st">&#39;active&#39;</span>]<span class="op">,</span></a>
<a class="sourceLine" id="cb12-7" title="7">    <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb12-8" title="8">  <span class="op">}</span></a>
<a class="sourceLine" id="cb12-9" title="9">)<span class="op">;</span></a>
<a class="sourceLine" id="cb12-10" title="10"></a>
<a class="sourceLine" id="cb12-11" title="11"><span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">query</span>(</a>
<a class="sourceLine" id="cb12-12" title="12">  <span class="st">&#39;SELECT *, &quot;text with literal $$1 and literal $$status&quot; as t FROM projects WHERE status = $status&#39;</span><span class="op">,</span></a>
<a class="sourceLine" id="cb12-13" title="13">  <span class="op">{</span></a>
<a class="sourceLine" id="cb12-14" title="14">    <span class="dt">bind</span><span class="op">:</span> <span class="op">{</span> <span class="dt">status</span><span class="op">:</span> <span class="st">&#39;active&#39;</span> <span class="op">},</span></a>
<a class="sourceLine" id="cb12-15" title="15">    <span class="dt">type</span><span class="op">:</span> <span class="va">QueryTypes</span>.<span class="at">SELECT</span></a>
<a class="sourceLine" id="cb12-16" title="16">  <span class="op">}</span></a>
<a class="sourceLine" id="cb12-17" title="17">)<span class="op">;</span></a></code></pre></div>
