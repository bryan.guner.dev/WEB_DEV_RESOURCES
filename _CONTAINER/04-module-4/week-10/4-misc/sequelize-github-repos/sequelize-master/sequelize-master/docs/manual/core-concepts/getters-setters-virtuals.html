<h1 id="getters-setters-virtuals">Getters, Setters &amp; Virtuals</h1>
<p>Sequelize allows you to define custom getters and setters for the attributes of your models.</p>
<p>Sequelize also allows you to specify the so-called <em>virtual attributes</em>, which are attributes on the Sequelize Model that doesn’t really exist in the underlying SQL table, but instead are populated automatically by Sequelize. They are very useful for simplifying code, for example.</p>
<h2 id="getters">Getters</h2>
<p>A getter is a <code>get()</code> function defined for one column in the model definition:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb1-1" title="1"><span class="kw">const</span> User <span class="op">=</span> <span class="va">sequelize</span>.<span class="at">define</span>(<span class="st">&#39;user&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb1-2" title="2">  <span class="co">// Let&#39;s say we wanted to see every username in uppercase, even</span></a>
<a class="sourceLine" id="cb1-3" title="3">  <span class="co">// though they are not necessarily uppercase in the database itself</span></a>
<a class="sourceLine" id="cb1-4" title="4">  <span class="dt">username</span><span class="op">:</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb1-5" title="5">    <span class="dt">type</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb1-6" title="6">    <span class="at">get</span>() <span class="op">{</span></a>
<a class="sourceLine" id="cb1-7" title="7">      <span class="kw">const</span> rawValue <span class="op">=</span> <span class="kw">this</span>.<span class="at">getDataValue</span>(username)<span class="op">;</span></a>
<a class="sourceLine" id="cb1-8" title="8">      <span class="cf">return</span> rawValue <span class="op">?</span> <span class="va">rawValue</span>.<span class="at">toUpperCase</span>() : <span class="kw">null</span><span class="op">;</span></a>
<a class="sourceLine" id="cb1-9" title="9">    <span class="op">}</span></a>
<a class="sourceLine" id="cb1-10" title="10">  <span class="op">}</span></a>
<a class="sourceLine" id="cb1-11" title="11"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>This getter, just like a standard JavaScript getter, is called automatically when the field value is read:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb2-1" title="1"><span class="kw">const</span> user <span class="op">=</span> <span class="va">User</span>.<span class="at">build</span>(<span class="op">{</span> <span class="dt">username</span><span class="op">:</span> <span class="st">&#39;SuperUser123&#39;</span> <span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb2-2" title="2"><span class="va">console</span>.<span class="at">log</span>(<span class="va">user</span>.<span class="at">username</span>)<span class="op">;</span> <span class="co">// &#39;SUPERUSER123&#39;</span></a>
<a class="sourceLine" id="cb2-3" title="3"><span class="va">console</span>.<span class="at">log</span>(<span class="va">user</span>.<span class="at">getDataValue</span>(username))<span class="op">;</span> <span class="co">// &#39;SuperUser123&#39;</span></a></code></pre></div>
<p>Note that, although <code>SUPERUSER123</code> was logged above, the value truly stored in the database is still <code>SuperUser123</code>. We used <code>this.getDataValue(username)</code> to obtain this value, and converted it to uppercase.</p>
<p>Had we tried to use <code>this.username</code> in the getter instead, we would have gotten an infinite loop! This is why Sequelize provides the <code>getDataValue</code> method.</p>
<h2 id="setters">Setters</h2>
<p>A setter is a <code>set()</code> function defined for one column in the model definition. It receives the value being set:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb3-1" title="1"><span class="kw">const</span> User <span class="op">=</span> <span class="va">sequelize</span>.<span class="at">define</span>(<span class="st">&#39;user&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb3-2" title="2">  <span class="dt">username</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb3-3" title="3">  <span class="dt">password</span><span class="op">:</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb3-4" title="4">    <span class="dt">type</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb3-5" title="5">    <span class="at">set</span>(value) <span class="op">{</span></a>
<a class="sourceLine" id="cb3-6" title="6">      <span class="co">// Storing passwords in plaintext in the database is terrible.</span></a>
<a class="sourceLine" id="cb3-7" title="7">      <span class="co">// Hashing the value with an appropriate cryptographic hash function is better.</span></a>
<a class="sourceLine" id="cb3-8" title="8">      <span class="kw">this</span>.<span class="at">setDataValue</span>(<span class="st">&#39;password&#39;</span><span class="op">,</span> <span class="at">hash</span>(value))<span class="op">;</span></a>
<a class="sourceLine" id="cb3-9" title="9">    <span class="op">}</span></a>
<a class="sourceLine" id="cb3-10" title="10">  <span class="op">}</span></a>
<a class="sourceLine" id="cb3-11" title="11"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<div class="sourceCode" id="cb4"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb4-1" title="1"><span class="kw">const</span> user <span class="op">=</span> <span class="va">User</span>.<span class="at">build</span>(<span class="op">{</span> <span class="dt">username</span><span class="op">:</span> <span class="st">&#39;someone&#39;</span><span class="op">,</span> <span class="dt">password</span><span class="op">:</span> <span class="st">&#39;NotSo§tr0ngP4$SW0RD!&#39;</span> <span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-2" title="2"><span class="va">console</span>.<span class="at">log</span>(<span class="va">user</span>.<span class="at">password</span>)<span class="op">;</span> <span class="co">// &#39;7cfc84b8ea898bb72462e78b4643cfccd77e9f05678ec2ce78754147ba947acc&#39;</span></a>
<a class="sourceLine" id="cb4-3" title="3"><span class="va">console</span>.<span class="at">log</span>(<span class="va">user</span>.<span class="at">getDataValue</span>(password))<span class="op">;</span> <span class="co">// &#39;7cfc84b8ea898bb72462e78b4643cfccd77e9f05678ec2ce78754147ba947acc&#39;</span></a></code></pre></div>
<p>Observe that Sequelize called the setter automatically, before even sending data to the database. The only data the database ever saw was the already hashed value.</p>
<p>If we wanted to involve another field from our model instance in the computation, that is possible and very easy!</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb5-1" title="1"><span class="kw">const</span> User <span class="op">=</span> <span class="va">sequelize</span>.<span class="at">define</span>(<span class="st">&#39;user&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb5-2" title="2">  <span class="dt">username</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb5-3" title="3">  <span class="dt">password</span><span class="op">:</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb5-4" title="4">    <span class="dt">type</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb5-5" title="5">    <span class="at">set</span>(value) <span class="op">{</span></a>
<a class="sourceLine" id="cb5-6" title="6">      <span class="co">// Storing passwords in plaintext in the database is terrible.</span></a>
<a class="sourceLine" id="cb5-7" title="7">      <span class="co">// Hashing the value with an appropriate cryptographic hash function is better.</span></a>
<a class="sourceLine" id="cb5-8" title="8">      <span class="co">// Using the username as a salt is better.</span></a>
<a class="sourceLine" id="cb5-9" title="9">      <span class="kw">this</span>.<span class="at">setDataValue</span>(<span class="st">&#39;password&#39;</span><span class="op">,</span> <span class="at">hash</span>(<span class="kw">this</span>.<span class="at">username</span> <span class="op">+</span> value))<span class="op">;</span></a>
<a class="sourceLine" id="cb5-10" title="10">    <span class="op">}</span></a>
<a class="sourceLine" id="cb5-11" title="11">  <span class="op">}</span></a>
<a class="sourceLine" id="cb5-12" title="12"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p><strong>Note:</strong> The above examples involving password handling, although much better than simply storing the password in plaintext, are far from perfect security. Handling passwords properly is hard, everything here is just for the sake of an example to show Sequelize functionality. We suggest involving a cybersecurity expert and/or reading <a href="https://www.owasp.org/">OWASP</a> documents and/or visiting the <a href="https://security.stackexchange.com/">InfoSec StackExchange</a>.</p>
<h2 id="combining-getters-and-setters">Combining getters and setters</h2>
<p>Getters and setters can be both defined in the same field.</p>
<p>For the sake of an example, let’s say we are modeling a <code>Post</code>, whose <code>content</code> is a text of unlimited length. To improve memory usage, let’s say we want to store a gzipped version of the content.</p>
<p><em>Note: modern databases should do some compression automatically in these cases. Please note that this is just for the sake of an example.</em></p>
<div class="sourceCode" id="cb6"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb6-1" title="1"><span class="kw">const</span> <span class="op">{</span> gzipSync<span class="op">,</span> gunzipSync <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;zlib&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb6-2" title="2"></a>
<a class="sourceLine" id="cb6-3" title="3"><span class="kw">const</span> Post <span class="op">=</span> <span class="va">sequelize</span>.<span class="at">define</span>(<span class="st">&#39;post&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb6-4" title="4">  <span class="dt">content</span><span class="op">:</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb6-5" title="5">    <span class="dt">type</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">TEXT</span><span class="op">,</span></a>
<a class="sourceLine" id="cb6-6" title="6">    <span class="at">get</span>() <span class="op">{</span></a>
<a class="sourceLine" id="cb6-7" title="7">      <span class="kw">const</span> storedValue <span class="op">=</span> <span class="kw">this</span>.<span class="at">getDataValue</span>(<span class="st">&#39;content&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb6-8" title="8">      <span class="kw">const</span> gzippedBuffer <span class="op">=</span> <span class="va">Buffer</span>.<span class="at">from</span>(storedValue<span class="op">,</span> <span class="st">&#39;base64&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb6-9" title="9">      <span class="kw">const</span> unzippedBuffer <span class="op">=</span> <span class="at">gunzipSync</span>(gzippedBuffer)<span class="op">;</span></a>
<a class="sourceLine" id="cb6-10" title="10">      <span class="cf">return</span> <span class="va">unzippedBuffer</span>.<span class="at">toString</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb6-11" title="11">    <span class="op">},</span></a>
<a class="sourceLine" id="cb6-12" title="12">    <span class="at">set</span>(value) <span class="op">{</span></a>
<a class="sourceLine" id="cb6-13" title="13">      <span class="kw">const</span> gzippedBuffer <span class="op">=</span> <span class="at">gzipSync</span>(value)<span class="op">;</span></a>
<a class="sourceLine" id="cb6-14" title="14">      <span class="kw">this</span>.<span class="at">setDataValue</span>(<span class="st">&#39;content&#39;</span><span class="op">,</span> <span class="va">gzippedBuffer</span>.<span class="at">toString</span>(<span class="st">&#39;base64&#39;</span>))<span class="op">;</span></a>
<a class="sourceLine" id="cb6-15" title="15">    <span class="op">}</span></a>
<a class="sourceLine" id="cb6-16" title="16">  <span class="op">}</span></a>
<a class="sourceLine" id="cb6-17" title="17"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>With the above setup, whenever we try to interact with the <code>content</code> field of our <code>Post</code> model, Sequelize will automatically handle the custom getter and setter. For example:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb7-1" title="1"><span class="kw">const</span> post <span class="op">=</span> <span class="cf">await</span> <span class="va">Post</span>.<span class="at">create</span>(<span class="op">{</span> <span class="dt">content</span><span class="op">:</span> <span class="st">&#39;Hello everyone!&#39;</span> <span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb7-2" title="2"></a>
<a class="sourceLine" id="cb7-3" title="3"><span class="va">console</span>.<span class="at">log</span>(<span class="va">post</span>.<span class="at">content</span>)<span class="op">;</span> <span class="co">// &#39;Hello everyone!&#39;</span></a>
<a class="sourceLine" id="cb7-4" title="4"><span class="co">// Everything is happening under the hood, so we can even forget that the</span></a>
<a class="sourceLine" id="cb7-5" title="5"><span class="co">// content is actually being stored as a gzipped base64 string!</span></a>
<a class="sourceLine" id="cb7-6" title="6"></a>
<a class="sourceLine" id="cb7-7" title="7"><span class="co">// However, if we are really curious, we can get the &#39;raw&#39; data...</span></a>
<a class="sourceLine" id="cb7-8" title="8"><span class="va">console</span>.<span class="at">log</span>(<span class="va">post</span>.<span class="at">getDataValue</span>(<span class="st">&#39;content&#39;</span>))<span class="op">;</span></a>
<a class="sourceLine" id="cb7-9" title="9"><span class="co">// Output: &#39;H4sIAAAAAAAACvNIzcnJV0gtSy2qzM9LVQQAUuk9jQ8AAAA=&#39;</span></a></code></pre></div>
<h2 id="virtual-fields">Virtual fields</h2>
<p>Virtual fields are fields that Sequelize populates under the hood, but in reality they don’t even exist in the database.</p>
<p>For example, let’s say we have the <code>firstName</code> and <code>lastName</code> attributes for a User.</p>
<p><em>Again, this is <a href="https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/">only for the sake of an example</a>.</em></p>
<p>It would be nice to have a simple way to obtain the <em>full name</em> directly! We can combine the idea of <code>getters</code> with the special data type Sequelize provides for this kind of situation: <code>DataTypes.VIRTUAL</code>:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb8-1" title="1"><span class="kw">const</span> <span class="op">{</span> DataTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&quot;sequelize&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb8-2" title="2"></a>
<a class="sourceLine" id="cb8-3" title="3"><span class="kw">const</span> User <span class="op">=</span> <span class="va">sequelize</span>.<span class="at">define</span>(<span class="st">&#39;user&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb8-4" title="4">  <span class="dt">firstName</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">TEXT</span><span class="op">,</span></a>
<a class="sourceLine" id="cb8-5" title="5">  <span class="dt">lastName</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">TEXT</span><span class="op">,</span></a>
<a class="sourceLine" id="cb8-6" title="6">  <span class="dt">fullName</span><span class="op">:</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb8-7" title="7">    <span class="dt">type</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">VIRTUAL</span><span class="op">,</span></a>
<a class="sourceLine" id="cb8-8" title="8">    <span class="at">get</span>() <span class="op">{</span></a>
<a class="sourceLine" id="cb8-9" title="9">      <span class="cf">return</span> <span class="vs">`</span><span class="sc">${</span><span class="kw">this</span>.<span class="at">firstName</span><span class="sc">}</span><span class="vs"> </span><span class="sc">${</span><span class="kw">this</span>.<span class="at">lastName</span><span class="sc">}</span><span class="vs">`</span><span class="op">;</span></a>
<a class="sourceLine" id="cb8-10" title="10">    <span class="op">},</span></a>
<a class="sourceLine" id="cb8-11" title="11">    <span class="at">set</span>(value) <span class="op">{</span></a>
<a class="sourceLine" id="cb8-12" title="12">      <span class="cf">throw</span> <span class="kw">new</span> <span class="at">Error</span>(<span class="st">&#39;Do not try to set the `fullName` value!&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb8-13" title="13">    <span class="op">}</span></a>
<a class="sourceLine" id="cb8-14" title="14">  <span class="op">}</span></a>
<a class="sourceLine" id="cb8-15" title="15"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>The <code>VIRTUAL</code> field does not cause a column in the table to exist. In other words, the model above will not have a <code>fullName</code> column. However, it will appear to have it!</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb9-1" title="1"><span class="kw">const</span> user <span class="op">=</span> <span class="cf">await</span> <span class="va">User</span>.<span class="at">create</span>(<span class="op">{</span> <span class="dt">firstName</span><span class="op">:</span> <span class="st">&#39;John&#39;</span><span class="op">,</span> <span class="dt">lastName</span><span class="op">:</span> <span class="st">&#39;Doe&#39;</span> <span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb9-2" title="2"><span class="va">console</span>.<span class="at">log</span>(<span class="va">user</span>.<span class="at">fullName</span>)<span class="op">;</span> <span class="co">// &#39;John Doe&#39;</span></a></code></pre></div>
<h2 id="gettermethods-and-settermethods"><code>getterMethods</code> and <code>setterMethods</code></h2>
<p>Sequelize also provides the <code>getterMethods</code> and <code>setterMethods</code> options in the model definition to specify things that look like, but aren’t exactly the same as, virtual attributes. This usage is discouraged and likely to be deprecated in the future (in favor of using virtual attributes directly).</p>
<p>Example:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb10-1" title="1"><span class="kw">const</span> <span class="op">{</span> Sequelize<span class="op">,</span> DataTypes <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;sequelize&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-2" title="2"><span class="kw">const</span> sequelize <span class="op">=</span> <span class="kw">new</span> <span class="at">Sequelize</span>(<span class="st">&#39;sqlite::memory:&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-3" title="3"></a>
<a class="sourceLine" id="cb10-4" title="4"><span class="kw">const</span> User <span class="op">=</span> <span class="va">sequelize</span>.<span class="at">define</span>(<span class="st">&#39;user&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb10-5" title="5">  <span class="dt">firstName</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb10-6" title="6">  <span class="dt">lastName</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span></a>
<a class="sourceLine" id="cb10-7" title="7"><span class="op">},</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb10-8" title="8">  <span class="dt">getterMethods</span><span class="op">:</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb10-9" title="9">    <span class="at">fullName</span>() <span class="op">{</span></a>
<a class="sourceLine" id="cb10-10" title="10">      <span class="cf">return</span> <span class="kw">this</span>.<span class="at">firstName</span> <span class="op">+</span> <span class="st">&#39; &#39;</span> <span class="op">+</span> <span class="kw">this</span>.<span class="at">lastName</span><span class="op">;</span></a>
<a class="sourceLine" id="cb10-11" title="11">    <span class="op">}</span></a>
<a class="sourceLine" id="cb10-12" title="12">  <span class="op">},</span></a>
<a class="sourceLine" id="cb10-13" title="13">  <span class="dt">setterMethods</span><span class="op">:</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb10-14" title="14">    <span class="at">fullName</span>(value) <span class="op">{</span></a>
<a class="sourceLine" id="cb10-15" title="15">      <span class="co">// Note: this is just for demonstration.</span></a>
<a class="sourceLine" id="cb10-16" title="16">      <span class="co">// See: https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/</span></a>
<a class="sourceLine" id="cb10-17" title="17">      <span class="kw">const</span> names <span class="op">=</span> <span class="va">value</span>.<span class="at">split</span>(<span class="st">&#39; &#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-18" title="18">      <span class="kw">const</span> firstName <span class="op">=</span> names[<span class="dv">0</span>]<span class="op">;</span></a>
<a class="sourceLine" id="cb10-19" title="19">      <span class="kw">const</span> lastName <span class="op">=</span> <span class="va">names</span>.<span class="at">slice</span>(<span class="dv">1</span>).<span class="at">join</span>(<span class="st">&#39; &#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-20" title="20">      <span class="kw">this</span>.<span class="at">setDataValue</span>(<span class="st">&#39;firstName&#39;</span><span class="op">,</span> firstName)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-21" title="21">      <span class="kw">this</span>.<span class="at">setDataValue</span>(<span class="st">&#39;lastName&#39;</span><span class="op">,</span> lastName)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-22" title="22">    <span class="op">}</span></a>
<a class="sourceLine" id="cb10-23" title="23">  <span class="op">}</span></a>
<a class="sourceLine" id="cb10-24" title="24"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-25" title="25"></a>
<a class="sourceLine" id="cb10-26" title="26">(<span class="kw">async</span> () <span class="kw">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb10-27" title="27">  <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">sync</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb10-28" title="28">  <span class="kw">let</span> user <span class="op">=</span> <span class="cf">await</span> <span class="va">User</span>.<span class="at">create</span>(<span class="op">{</span> <span class="dt">firstName</span><span class="op">:</span> <span class="st">&#39;John&#39;</span><span class="op">,</span>  <span class="dt">lastName</span><span class="op">:</span> <span class="st">&#39;Doe&#39;</span> <span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-29" title="29">  <span class="va">console</span>.<span class="at">log</span>(<span class="va">user</span>.<span class="at">fullName</span>)<span class="op">;</span> <span class="co">// &#39;John Doe&#39;</span></a>
<a class="sourceLine" id="cb10-30" title="30">  <span class="va">user</span>.<span class="at">fullName</span> <span class="op">=</span> <span class="st">&#39;Someone Else&#39;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb10-31" title="31">  <span class="cf">await</span> <span class="va">user</span>.<span class="at">save</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb10-32" title="32">  user <span class="op">=</span> <span class="cf">await</span> <span class="va">User</span>.<span class="at">findOne</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb10-33" title="33">  <span class="va">console</span>.<span class="at">log</span>(<span class="va">user</span>.<span class="at">firstName</span>)<span class="op">;</span> <span class="co">// &#39;Someone&#39;</span></a>
<a class="sourceLine" id="cb10-34" title="34">  <span class="va">console</span>.<span class="at">log</span>(<span class="va">user</span>.<span class="at">lastName</span>)<span class="op">;</span> <span class="co">// &#39;Else&#39;</span></a>
<a class="sourceLine" id="cb10-35" title="35"><span class="op">}</span>)()<span class="op">;</span></a></code></pre></div>
