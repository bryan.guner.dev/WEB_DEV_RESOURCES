<h1 id="joins-vs.-subqueries">Joins vs. Subqueries</h1>
<hr />
<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->
<ul>
<li><a href="#joins-vs-subqueries">Joins vs. Subqueries</a>
<ul>
<li><a href="#what-is-a-join">What is a JOIN?</a></li>
<li><a href="#what-is-a-subquery">What is a subquery?</a>
<ul>
<li><a href="#single-value-subquery">Single-value subquery</a></li>
<li><a href="#multiple-row-subquery">Multiple-row subquery</a></li>
</ul></li>
<li><a href="#using-joins-vs-subqueries">Using joins vs. subqueries</a>
<ul>
<li><a href="#should-i-use-a-join-or-a-subquery">Should I use a JOIN or a subquery?</a></li>
</ul></li>
<li><a href="#helpful-links">Helpful links:</a></li>
</ul></li>
</ul>
<!-- /code_chunk_output -->
<hr />
<p>To select, or not to select? That is the query. We’ve barely scratched the surface of SQL queries. Previously, we went over how to write simple SQL queries using the <code>SELECT</code> statement, and we learned how to incorporate a <code>WHERE</code> clause into our queries.</p>
<p>There’s a lot more we could add to our queries to get more refined results. In this reading, we’ll go over joins and subqueries and talk about when we would use one over the other.</p>
<h2 id="what-is-a-join">What is a JOIN?</h2>
<p>We briefly looked at the <code>JOIN</code> operation after we created foreign keys in a previous reading. The <a href="https://www.postgresql.org/docs/8.3/tutorial-join.html">JOIN operation</a> allows us to retrieve rows from multiple tables.</p>
<p>To review, we had two tables: a “breeds” table and a “puppies” table. The two tables shared information through a foreign key. The foreign key <code>breed_id</code> lives on the “puppies” table and is related to the primary key <code>id</code> of the “breeds” table.</p>
<p>We wrote the following <code>INNER JOIN</code> operation to get only the rows from the “puppies” table with a matching <code>breed_id</code> in the “friends” table:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb1-1" title="1"><span class="kw">SELECT</span> <span class="op">*</span> <span class="kw">FROM</span> puppies</a>
<a class="sourceLine" id="cb1-2" title="2"><span class="kw">INNER</span> <span class="kw">JOIN</span> breeds <span class="kw">ON</span> (puppies.breed_id <span class="op">=</span> breeds.<span class="kw">id</span>);</a></code></pre></div>
<p><a href="http://www.postgresqltutorial.com/postgresql-inner-join/">INNER JOIN</a> can be represented as a Venn Diagram, which produces rows from Table A that match some information in Table B.</p>
<figure>
<img src="images/inner-join-venn-diagram.png" alt="inner-join-venn-diagram" /><figcaption>inner-join-venn-diagram</figcaption>
</figure>
<p>We got the following table rows back from our <code>INNER JOIN</code> on the “puppies” table. These rows represent the center overlapping area of the two circles. We can see that the data from “puppies” appears first, followed by the joined data from the “friends” table.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb2-1" title="1"> <span class="kw">id</span> |   name   | age_yrs | breed_id | weight_lbs | microchipped | <span class="kw">id</span> |        name</a>
<a class="sourceLine" id="cb2-2" title="2"><span class="co">----+----------+---------+----------+------------+--------------+----+---------------------</span></a>
<a class="sourceLine" id="cb2-3" title="3">  <span class="dv">1</span> | Cooper   |     <span class="fl">1.0</span> |        <span class="dv">8</span> |         <span class="dv">18</span> | t            |  <span class="dv">8</span> | Miniature Schnauzer</a>
<a class="sourceLine" id="cb2-4" title="4">  <span class="dv">2</span> | Indie    |     <span class="fl">0.5</span> |        <span class="dv">9</span> |         <span class="dv">13</span> | t            |  <span class="dv">9</span> | Yorkshire Terrier</a>
<a class="sourceLine" id="cb2-5" title="5">  <span class="dv">3</span> | Kota     |     <span class="fl">0.7</span> |        <span class="dv">1</span> |         <span class="dv">26</span> | f            |  <span class="dv">1</span> | Australian Shepherd</a>
<a class="sourceLine" id="cb2-6" title="6">  <span class="dv">4</span> | Zoe      |     <span class="fl">0.8</span> |        <span class="dv">6</span> |         <span class="dv">32</span> | t            |  <span class="dv">6</span> | Korean Jindo</a>
<a class="sourceLine" id="cb2-7" title="7">  <span class="dv">5</span> | Charley  |     <span class="fl">1.5</span> |        <span class="dv">2</span> |         <span class="dv">25</span> | f            |  <span class="dv">2</span> | Basset Hound</a>
<a class="sourceLine" id="cb2-8" title="8">  <span class="dv">6</span> | Ladybird |     <span class="fl">0.6</span> |        <span class="dv">7</span> |         <span class="dv">20</span> | t            |  <span class="dv">7</span> | Labradoodle</a>
<a class="sourceLine" id="cb2-9" title="9">  <span class="dv">7</span> | Callie   |     <span class="fl">0.9</span> |        <span class="dv">4</span> |         <span class="dv">16</span> | f            |  <span class="dv">4</span> | Corgi</a>
<a class="sourceLine" id="cb2-10" title="10">  <span class="dv">8</span> | Jaxson   |     <span class="fl">0.4</span> |        <span class="dv">3</span> |         <span class="dv">19</span> | t            |  <span class="dv">3</span> | Beagle</a>
<a class="sourceLine" id="cb2-11" title="11">  <span class="dv">9</span> | Leinni   |     <span class="fl">1.0</span> |        <span class="dv">8</span> |         <span class="dv">25</span> | t            |  <span class="dv">8</span> | Miniature Schnauzer</a>
<a class="sourceLine" id="cb2-12" title="12"> <span class="dv">10</span> | <span class="fu">Max</span>      |     <span class="fl">1.6</span> |        <span class="dv">5</span> |         <span class="dv">65</span> | f            |  <span class="dv">5</span> | German Shepherd</a></code></pre></div>
<p>There are different types of <code>JOIN</code> operations. The ones you’ll use most often are:</p>
<ol type="1">
<li><strong>Inner Join</strong> – Returns a result set containing rows in the left table that match rows in the right table.</li>
<li><strong>Left Join</strong> – Returns a result set containing all rows from the left table with the matching rows from the right table. If there is no match, the right side will have null values.</li>
<li><strong>Right Join</strong> – Returns a result set containing all rows from the right table with matching rows from the left table. If there is no match, the left side will have null values.</li>
<li><strong>Full Outer Join</strong> – Returns a result set containing all rows from both the left and right tables, with the matching rows from both sides where available. If there is no match, the missing side contains null values.</li>
<li><strong>Self-Join</strong> – A self-join is a query in which a table is joined to itself. Self-joins are useful for comparing values in a column of rows within the same table.</li>
</ol>
<p>(<em>See this tutorial doc on <a href="http://www.postgresqltutorial.com/postgresql-joins/">PostgreSQL Joins</a> for more information on the different <code>JOIN</code> operations.</em>)</p>
<h2 id="what-is-a-subquery">What is a subquery?</h2>
<p>A subquery is essentially a <code>SELECT</code> statement nested inside another <code>SELECT</code> statement. A subquery can return a single (“scalar”) value or multiple rows.</p>
<h3 id="single-value-subquery">Single-value subquery</h3>
<p>Let’s see an example of how to use a subquery to return a single value. Take the “puppies” table from before. We had a column called <code>age_yrs</code> in that table (see below).</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb3-1" title="1">postgres<span class="op">=</span># <span class="kw">SELECT</span> <span class="op">*</span> <span class="kw">FROM</span> puppies;</a>
<a class="sourceLine" id="cb3-2" title="2"> <span class="kw">id</span> |   name   | age_yrs | breed_id | weight_lbs | microchipped</a>
<a class="sourceLine" id="cb3-3" title="3"><span class="co">----+----------+---------+----------+------------+--------------</span></a>
<a class="sourceLine" id="cb3-4" title="4">  <span class="dv">1</span> | Cooper   |     <span class="fl">1.0</span> |        <span class="dv">8</span> |         <span class="dv">18</span> | t</a>
<a class="sourceLine" id="cb3-5" title="5">  <span class="dv">2</span> | Indie    |     <span class="fl">0.5</span> |        <span class="dv">9</span> |         <span class="dv">13</span> | t</a>
<a class="sourceLine" id="cb3-6" title="6">  <span class="dv">3</span> | Kota     |     <span class="fl">0.7</span> |        <span class="dv">1</span> |         <span class="dv">26</span> | f</a>
<a class="sourceLine" id="cb3-7" title="7">  <span class="dv">4</span> | Zoe      |     <span class="fl">0.8</span> |        <span class="dv">6</span> |         <span class="dv">32</span> | t</a>
<a class="sourceLine" id="cb3-8" title="8">  <span class="dv">5</span> | Charley  |     <span class="fl">1.5</span> |        <span class="dv">2</span> |         <span class="dv">25</span> | f</a>
<a class="sourceLine" id="cb3-9" title="9">  <span class="dv">6</span> | Ladybird |     <span class="fl">0.6</span> |        <span class="dv">7</span> |         <span class="dv">20</span> | t</a>
<a class="sourceLine" id="cb3-10" title="10">  <span class="dv">7</span> | Callie   |     <span class="fl">0.9</span> |        <span class="dv">4</span> |         <span class="dv">16</span> | f</a>
<a class="sourceLine" id="cb3-11" title="11">  <span class="dv">8</span> | Jaxson   |     <span class="fl">0.4</span> |        <span class="dv">3</span> |         <span class="dv">19</span> | t</a>
<a class="sourceLine" id="cb3-12" title="12">  <span class="dv">9</span> | Leinni   |     <span class="fl">1.0</span> |        <span class="dv">8</span> |         <span class="dv">25</span> | t</a>
<a class="sourceLine" id="cb3-13" title="13"> <span class="dv">10</span> | <span class="fu">Max</span>      |     <span class="fl">1.6</span> |        <span class="dv">5</span> |         <span class="dv">65</span> | f</a>
<a class="sourceLine" id="cb3-14" title="14">(<span class="dv">10</span> <span class="kw">rows</span>)</a></code></pre></div>
<p>We’ll use the <a href="https://www.postgresql.org/docs/8.2/functions-aggregate.html">PostgreSQL aggregate function</a> <code>AVG</code> to get an average puppy age.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb4-1" title="1"><span class="kw">SELECT</span></a>
<a class="sourceLine" id="cb4-2" title="2">  <span class="fu">AVG</span> (age_yrs)</a>
<a class="sourceLine" id="cb4-3" title="3"><span class="kw">FROM</span></a>
<a class="sourceLine" id="cb4-4" title="4">  puppies;</a></code></pre></div>
<p>Assuming our previous “puppies” table still exists in our database, if we entered the above statement into psql we’d get an <em><strong>average age of 0.9</strong></em>. (<em>Note: Try it out yourself in psql! Refer to the reading “Retrieving Rows From A Table Using SELECT” if you need help remembering how we set up the “puppies” table.</em>)</p>
<p>Let’s say that we wanted to find all of the puppies that are older than the average age of 0.9. We could write the following query:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb5-1" title="1"><span class="kw">SELECT</span></a>
<a class="sourceLine" id="cb5-2" title="2">  name,</a>
<a class="sourceLine" id="cb5-3" title="3">  age_yrs,</a>
<a class="sourceLine" id="cb5-4" title="4">  breed</a>
<a class="sourceLine" id="cb5-5" title="5"><span class="kw">FROM</span></a>
<a class="sourceLine" id="cb5-6" title="6">  puppies</a>
<a class="sourceLine" id="cb5-7" title="7"><span class="kw">WHERE</span></a>
<a class="sourceLine" id="cb5-8" title="8">  age_yrs <span class="op">&gt;</span> <span class="fl">0.9</span>;</a></code></pre></div>
<p>In the above query, we compared <code>age_yrs</code> to an actual number (0.9). However, as more puppies get added to our table, the average age could change at any time. To make our statement more dynamic, we can plug in the query we wrote to find the average age into another statement as a <em>subquery</em> (surrounded by parentheses).</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb6-1" title="1"><span class="kw">SELECT</span></a>
<a class="sourceLine" id="cb6-2" title="2">  puppies.name,</a>
<a class="sourceLine" id="cb6-3" title="3">  age_yrs,</a>
<a class="sourceLine" id="cb6-4" title="4">  breeds.name</a>
<a class="sourceLine" id="cb6-5" title="5"><span class="kw">FROM</span></a>
<a class="sourceLine" id="cb6-6" title="6">  puppies</a>
<a class="sourceLine" id="cb6-7" title="7"><span class="kw">INNER</span> <span class="kw">JOIN</span></a>
<a class="sourceLine" id="cb6-8" title="8">  breeds <span class="kw">ON</span> (breeds.<span class="kw">id</span> <span class="op">=</span> puppies.breed_id)</a>
<a class="sourceLine" id="cb6-9" title="9"><span class="kw">WHERE</span></a>
<a class="sourceLine" id="cb6-10" title="10">  age_yrs <span class="op">&gt;</span> (</a>
<a class="sourceLine" id="cb6-11" title="11">    <span class="kw">SELECT</span></a>
<a class="sourceLine" id="cb6-12" title="12">      <span class="fu">AVG</span> (age_yrs)</a>
<a class="sourceLine" id="cb6-13" title="13">    <span class="kw">FROM</span></a>
<a class="sourceLine" id="cb6-14" title="14">      puppies</a>
<a class="sourceLine" id="cb6-15" title="15">  );</a></code></pre></div>
<p>We should get the following table rows, which include only the puppies older than 9 months:</p>
<pre class="shell"><code>  name   | age_yrs |        breed
---------+---------+---------------------
 Cooper  |     1.0 | Miniature Schnauzer
 Charley |     1.5 | Basset Hound
 Leinni  |     1.0 | Miniature Schnauzer
 Max     |     1.6 | German Shepherd</code></pre>
<h3 id="multiple-row-subquery">Multiple-row subquery</h3>
<p>We could also write a subquery that returns multiple rows.</p>
<p>In the reading “Creating A Table In An Existing PostgreSQL Database”, we created a “friends” table. In “Foreign Keys And The JOIN Operation”, we set up a primary key in the “puppies” table that is a foreign key in the “friends” table – <code>puppy_id</code>. We’ll use this ID in our subquery and outer query.</p>
<p><strong>“friends” table</strong></p>
<pre class="shell"><code>id | first_name | last_name | puppy_id
----+------------+-----------+----------
  1 | Amy        | Pond      |        4
  2 | Rose       | Tyler     |        5
  3 | Martha     | Jones     |        6
  4 | Donna      | Noble     |        7
  5 | River      | Song      |        8</code></pre>
<p>Let’s say we wanted to find all the puppies that are younger than 6 months old.</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb9-1" title="1"><span class="kw">SELECT</span> puppy_id</a>
<a class="sourceLine" id="cb9-2" title="2"><span class="kw">FROM</span> puppies</a>
<a class="sourceLine" id="cb9-3" title="3"><span class="kw">WHERE</span></a>
<a class="sourceLine" id="cb9-4" title="4">  age_yrs <span class="op">&lt;</span> <span class="fl">0.6</span>;</a></code></pre></div>
<p>This would return two rows:</p>
<pre class="shell"><code>puppy_id
----------
        2
        8
(2 rows)</code></pre>
<p>Now we want to use the above statement as a subquery (inside parentheses) in another query. You’ll notice we’re using a <code>WHERE</code> clause with the <a href="http://www.postgresqltutorial.com/postgresql-in/">IN</a> operator to check if the <code>puppy_id</code> from the “friends” table meets the conditions in the subquery.</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb11-1" title="1"><span class="kw">SELECT</span> <span class="op">*</span></a>
<a class="sourceLine" id="cb11-2" title="2"><span class="kw">FROM</span> friends</a>
<a class="sourceLine" id="cb11-3" title="3"><span class="kw">WHERE</span></a>
<a class="sourceLine" id="cb11-4" title="4">  puppy_id <span class="kw">IN</span> (</a>
<a class="sourceLine" id="cb11-5" title="5">    <span class="kw">SELECT</span> puppy_id</a>
<a class="sourceLine" id="cb11-6" title="6">    <span class="kw">FROM</span> puppies</a>
<a class="sourceLine" id="cb11-7" title="7">    <span class="kw">WHERE</span></a>
<a class="sourceLine" id="cb11-8" title="8">      age_yrs <span class="op">&lt;</span> <span class="fl">0.6</span></a>
<a class="sourceLine" id="cb11-9" title="9">  );</a></code></pre></div>
<p>We should get just one row back. River Song has a puppy younger than 6 months old.</p>
<pre class="shell"><code> id | first_name | last_name | puppy_id
----+------------+-----------+----------
  5 | River      | Song      |        8
(1 row)</code></pre>
<h2 id="using-joins-vs.-subqueries">Using joins vs. subqueries</h2>
<p>We can use either a <code>JOIN</code> operation or a subquery to filter for the same information. Both methods can be used to get info from multiple tables. Take the query/subquery from above:</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb13-1" title="1"><span class="kw">SELECT</span> <span class="op">*</span></a>
<a class="sourceLine" id="cb13-2" title="2"><span class="kw">FROM</span> friends</a>
<a class="sourceLine" id="cb13-3" title="3"><span class="kw">WHERE</span></a>
<a class="sourceLine" id="cb13-4" title="4">  puppy_id <span class="kw">IN</span>  (</a>
<a class="sourceLine" id="cb13-5" title="5">    <span class="kw">SELECT</span> puppy_id</a>
<a class="sourceLine" id="cb13-6" title="6">    <span class="kw">FROM</span> puppies</a>
<a class="sourceLine" id="cb13-7" title="7">    <span class="kw">WHERE</span></a>
<a class="sourceLine" id="cb13-8" title="8">      age_yrs <span class="op">&lt;</span> <span class="fl">0.6</span></a>
<a class="sourceLine" id="cb13-9" title="9">  );</a></code></pre></div>
<p>Which produced the following result:</p>
<pre class="shell"><code> id | first_name | last_name | puppy_id
----+------------+-----------+----------
  5 | River      | Song      |        8
(1 row)</code></pre>
<p>Instead of using a <code>WHERE</code> clause with a subquery, we could use a <code>JOIN</code> operation:</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb15-1" title="1"><span class="kw">SELECT</span> <span class="op">*</span></a>
<a class="sourceLine" id="cb15-2" title="2"><span class="kw">FROM</span> friends</a>
<a class="sourceLine" id="cb15-3" title="3"><span class="kw">INNER</span> <span class="kw">JOIN</span> puppies <span class="kw">ON</span> (puppies.puppy_id <span class="op">=</span> friends.puppy_id)</a>
<a class="sourceLine" id="cb15-4" title="4"><span class="kw">WHERE</span></a>
<a class="sourceLine" id="cb15-5" title="5">  puppies.age_yrs <span class="op">&lt;</span> <span class="fl">0.6</span>;</a></code></pre></div>
<p>Again, we’d get back one result, but because we used an <code>INNER JOIN</code> we have information from both the “puppies” and “friends” tables.</p>
<pre class="shell"><code>id | first_name | last_name | puppy_id |  name  | age_yrs | breed  | weight_lbs | microchipped | puppy_id
----+------------+-----------+----------+--------+---------+--------+------------+--------------+----------
  5 | River      | Song      |        8 | Jaxson |     0.4 | Beagle |         19 | t            |        8
(1 row)</code></pre>
<h3 id="should-i-use-a-join-or-a-subquery">Should I use a JOIN or a subquery?</h3>
<p>As stated earlier, we could use either a JOIN operation or a subquery to filter for table rows. However, you might want to think about whether using a JOIN or a subquery is more appropriate for retrieving data.</p>
<p>A JOIN operation is ideal when you want to combine rows from one or more tables based on a match condition. Subqueries work great when you’re returning a single value. When you’re returning multiple rows, you could opt for a subquery or a JOIN.</p>
<p>Executing a query using a JOIN could potentially be faster than executing a subquery that would return the same data. (A subquery will execute once for each row returned in the outer query, whereas the <code>INNER JOIN</code> only has to make one pass through the data.) However, this isn’t always the case. Performance depends on the size of your data, what you’re filtering for, and how the server optimizes the query. With smaller datasets, the difference in performance of a JOIN and subquery is imperceptible. However, there are use cases where a subquery would improve performance.</p>
<p>(<em>See this article for more info: <a href="https://crate.io/a/sql-subquery-vs-left-join/">When is a SQL Subquery 260x Faster than a Left Join?</a></em>)</p>
<p>We can use the the <a href="http://www.postgresqltutorial.com/postgresql-explain/">EXPLAIN</a> statement to see runtime statistics of our queries that help with debugging slow queries. (We’ll get into this more later!)</p>
<h2 id="helpful-links">Helpful links:</h2>
<ul>
<li>PostgreSQL Tutorial: <a href="http://www.postgresqltutorial.com/postgresql-joins/">PostgreSQL Joins</a></li>
<li>PostgreSQL Tutorial: <a href="http://www.postgresqltutorial.com/postgresql-subquery/">PostgreSQL Subquery</a></li>
<li>PostgreSQL Docs: <a href="https://www.postgresql.org/docs/8.3/functions-subquery.html">Subquery Expressions</a></li>
<li>Essential SQL: <a href="https://www.essentialsql.com/what-is-the-difference-between-a-join-and-subquery/">Subqueries versus Joins</a></li>
<li>Essential SQL: <a href="https://www.essentialsql.com/get-ready-to-learn-sql-server-20-using-subqueries-in-the-select-statement/">Using Subqueries with the Select Statement</a></li>
</ul>
