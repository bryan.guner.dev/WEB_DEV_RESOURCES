<h1 id="using-sql-transactions">Using SQL Transactions</h1>
<hr />
<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->
<ul>
<li><a href="#using-sql-transactions">Using SQL Transactions</a>
<ul>
<li><a href="#what-is-a-transaction">What is a transaction?</a></li>
<li><a href="#implicit-vs-explicit-transactions">Implicit vs. explicit transactions</a>
<ul>
<li><a href="#postgresql-transactional-commands">PostgreSQL transactional commands</a></li>
</ul></li>
<li><a href="#when-to-use-transactions-and-why">When to use transactions and why</a></li>
<li><a href="#transaction-properties-acid">Transaction properties: ACID</a>
<ul>
<li><a href="#banking-transaction-example">Banking transaction example</a></li>
</ul></li>
<li><a href="#helpful-links">Helpful links:</a></li>
</ul></li>
</ul>
<!-- /code_chunk_output -->
<hr />
<p>Transactions allow us to make changes to a SQL database in a consistent and durable way, and it’s a best practice to use them regularly.</p>
<p>In this reading, we’ll cover what a transaction is and why we want to use it, as well as how to write explicit transactions.</p>
<h2 id="what-is-a-transaction">What is a transaction?</h2>
<p>A transaction is a single unit of work, which can contain multiple operations, performed on a database. According to the <a href="https://www.postgresql.org/docs/8.3/tutorial-transactions.html">PostgreSQL docs</a>, the important thing to note about a transaction is that “it bundles multiple steps into a single, all-or-nothing operation”. If any operation within the transaction fails, then the entire transaction fails. If all the operations succeed, then the entire transaction succeeds.</p>
<h2 id="implicit-vs.-explicit-transactions">Implicit vs. explicit transactions</h2>
<p>Every SQL statement is effectively a transaction. When you insert a new table row into a database table, for example, you are creating a transaction. The following <code>INSERT</code> statement is a transaction:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb1-1" title="1"><span class="kw">INSERT</span> <span class="kw">INTO</span> hobbits(name,purpose)</a>
<a class="sourceLine" id="cb1-2" title="2">  <span class="kw">VALUES</span>(<span class="st">&#39;Frodo&#39;</span>,<span class="st">&#39;Destroy the One Ring of power.&#39;</span>);</a></code></pre></div>
<p>The above code is known as an <em>implicit</em> transaction. With an implicit transaction, changes to the database happen immediately, and we have no way to undo or roll back these changes. We can only make subsequent changes/ transactions.</p>
<p>An <em>explicit</em> transaction, however, allows us to create save points and roll back to whatever point in time we choose. An explicit transaction begins with the command <code>BEGIN</code>, followed by the SQL statement, and then ends with either a <code>COMMIT</code> or <code>ROLLBACK</code>.</p>
<h3 id="postgresql-transactional-commands">PostgreSQL transactional commands</h3>
<p><strong><a href="https://www.postgresql.org/docs/9.1/sql-begin.html">BEGIN</a></strong> – Initiates a transaction block. All statements after a BEGIN command will be executed in a single transaction until an explicit COMMIT or ROLLBACK is given.</p>
<p>Starting a transaction:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb2-1" title="1"><span class="cf">BEGIN</span>;</a>
<a class="sourceLine" id="cb2-2" title="2">  <span class="kw">INSERT</span> <span class="kw">INTO</span> hobbits(name,purpose)</a>
<a class="sourceLine" id="cb2-3" title="3">    <span class="kw">VALUES</span>(<span class="st">&#39;Frodo&#39;</span>,<span class="st">&#39;Destroy the One Ring of power.&#39;</span>);</a></code></pre></div>
<p><strong><a href="https://www.postgresql.org/docs/9.1/sql-commit.html">COMMIT</a></strong> – Commits the current transaction. All changes made by the transaction become visible to others and are guaranteed to be durable if a crash occurs.</p>
<p>Committing a transaction:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb3-1" title="1"><span class="cf">BEGIN</span>;</a>
<a class="sourceLine" id="cb3-2" title="2">  <span class="kw">INSERT</span> <span class="kw">INTO</span> hobbits(name,purpose)</a>
<a class="sourceLine" id="cb3-3" title="3">    <span class="kw">VALUES</span>(<span class="st">&#39;Frodo&#39;</span>,<span class="st">&#39;Destroy the One Ring of power.&#39;</span>);</a>
<a class="sourceLine" id="cb3-4" title="4"><span class="kw">COMMIT</span>;</a></code></pre></div>
<p><strong><a href="https://www.postgresql.org/docs/9.1/sql-rollback.html">ROLLBACK</a></strong> – Rolls back the current transaction and causes all the updates made by the transaction to be discarded. Can only undo transactions since the last COMMIT or ROLLBACK command was issued.</p>
<p>Rolling back a transaction (i.e. abort all changes):</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb4-1" title="1"><span class="cf">BEGIN</span>;</a>
<a class="sourceLine" id="cb4-2" title="2">  <span class="kw">INSERT</span> <span class="kw">INTO</span> hobbits(name,purpose)</a>
<a class="sourceLine" id="cb4-3" title="3">    <span class="kw">VALUES</span>(<span class="st">&#39;Frodo&#39;</span>,<span class="st">&#39;Destroy the One Ring of power.&#39;</span>);</a>
<a class="sourceLine" id="cb4-4" title="4"><span class="kw">ROLLBACK</span>;</a></code></pre></div>
<p><strong><a href="https://www.postgresql.org/docs/9.1/sql-savepoint.html">SAVEPOINT</a></strong> – Establishes a new save point within the current transaction. Allows all commands executed after the save point to be rolled back, restoring the transaction state to what it was at the time of the save point.</p>
<p>Syntax to create save point: <code>SAVEPOINT savepoint_name;</code> Syntax to delete a save point: <code>RELEASE SAVEPOINT savepoint_name;</code></p>
<p>Let’s say we had the following table called <code>fellowship</code>:</p>
<table>
<thead>
<tr class="header">
<th>name</th>
<th>age</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Frodo</td>
<td>50</td>
</tr>
<tr class="even">
<td>Samwise</td>
<td>38</td>
</tr>
<tr class="odd">
<td>Merry</td>
<td>36</td>
</tr>
<tr class="even">
<td>Pippin</td>
<td>28</td>
</tr>
<tr class="odd">
<td>Aragorn</td>
<td>87</td>
</tr>
<tr class="even">
<td>Boromir</td>
<td>40</td>
</tr>
<tr class="odd">
<td>Legolas</td>
<td>2000</td>
</tr>
<tr class="even">
<td>Gandalf</td>
<td>2000</td>
</tr>
</tbody>
</table>
<p>We’ll create a transaction on this table containing a few operations. Inside the transaction, we’ll establish a save point that we’ll roll back to before committing.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb5-1" title="1"><span class="cf">BEGIN</span>;</a>
<a class="sourceLine" id="cb5-2" title="2">  <span class="kw">DELETE</span> <span class="kw">FROM</span> fellowship</a>
<a class="sourceLine" id="cb5-3" title="3">    <span class="kw">WHERE</span> age <span class="op">&gt;</span> <span class="dv">100</span>;</a>
<a class="sourceLine" id="cb5-4" title="4">  <span class="kw">SAVEPOINT</span> first_savepoint;</a>
<a class="sourceLine" id="cb5-5" title="5">  <span class="kw">DELETE</span> <span class="kw">FROM</span> fellowship</a>
<a class="sourceLine" id="cb5-6" title="6">    <span class="kw">WHERE</span> age <span class="op">&gt;</span> <span class="dv">80</span>;</a>
<a class="sourceLine" id="cb5-7" title="7">  <span class="kw">DELETE</span> <span class="kw">FROM</span> fellowship</a>
<a class="sourceLine" id="cb5-8" title="8">    <span class="kw">WHERE</span> age <span class="op">&gt;=</span> <span class="dv">40</span>;</a>
<a class="sourceLine" id="cb5-9" title="9">  <span class="kw">ROLLBACK</span> <span class="kw">TO</span> first_savepoint;</a>
<a class="sourceLine" id="cb5-10" title="10"><span class="kw">COMMIT</span>;</a></code></pre></div>
<p>Once our transaction is committed, our table would look like this:</p>
<table>
<thead>
<tr class="header">
<th>name</th>
<th>age</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Frodo</td>
<td>50</td>
</tr>
<tr class="even">
<td>Samwise</td>
<td>38</td>
</tr>
<tr class="odd">
<td>Merry</td>
<td>36</td>
</tr>
<tr class="even">
<td>Pippin</td>
<td>28</td>
</tr>
<tr class="odd">
<td>Aragorn</td>
<td>87</td>
</tr>
<tr class="even">
<td>Boromir</td>
<td>40</td>
</tr>
</tbody>
</table>
<p>We can see that the deletion that happened just prior to the savepoint creation was preserved.</p>
<p><strong><a href="https://www.postgresql.org/docs/9.1/sql-set-transaction.html">SET TRANSACTION</a></strong> – Sets the characteristics of the current transaction. (_Note: To set characteristics for subsequent transactions in a session, use <code>SET SESSION   CHARACTERISTICS</code>.) The available transaction characteristics are the transaction isolation level, the transaction access mode (read/write or read-only), and the deferrable mode. (<em>Read more about these characteristics in the <a href="https://www.postgresql.org/docs/9.1/sql-set-transaction.html">PostgreSQL docs</a>.</em>)</p>
<p>Example of setting transaction characteristics:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb6-1" title="1"><span class="cf">BEGIN</span>;</a>
<a class="sourceLine" id="cb6-2" title="2">  <span class="kw">SET</span> <span class="kw">TRANSACTION</span> <span class="kw">READ</span> <span class="kw">ONLY</span>;</a>
<a class="sourceLine" id="cb6-3" title="3">  <span class="op">..</span>.</a>
<a class="sourceLine" id="cb6-4" title="4"><span class="kw">COMMIT</span>;</a></code></pre></div>
<h2 id="when-to-use-transactions-and-why">When to use transactions and why</h2>
<p>It is generally a good idea to use explicit SQL transactions when making any updates, insertions, or deletions, to a database. However, you generally wouldn’t write an explicit transaction for a simple <code>SELECT</code> query.</p>
<p>Transactions help you deal with crashes, failures, data consistency, and error handling. The ability to create savepoints and roll back to earlier points is tremendously helpful when doing multiple updates and helps maintain data integrity.</p>
<p>Another benefit of transactions is the <em><strong>atomic</strong></em>, or “all-or-nothing”, nature of their operations. Because all of the operations in a transaction must succeed or else be aborted, partial or incomplete updates to the database will not be made. End-users will see only the final result of the transaction.</p>
<h2 id="transaction-properties-acid">Transaction properties: ACID</h2>
<p>A SQL transaction has four properties known collectively as “ACID” – which is an acronym for <em>Atomic, Consistent, Isolated, and Durable</em>. The following descriptions come from the IBM doc “<a href="https://www.ibm.com/support/knowledgecenter/en/SSGMCP_5.4.0/product-overview/acid.html">ACID properties of transactions</a>”:</p>
<p><strong>Atomicity</strong> – All changes to data are performed as if they are a single operation. That is, all the changes are performed, or none of them are.</p>
<p>For example, in an application that transfers funds from one account to another, the atomicity property ensures that, if a debit is made successfully from one account, the corresponding credit is made to the other account.</p>
<p><strong>Consistency</strong> – Data is in a consistent state when a transaction starts and when it ends.</p>
<p>For example, in an application that transfers funds from one account to another, the consistency property ensures that the total value of funds in both the accounts is the same at the start and end of each transaction.</p>
<p><strong>Isolation</strong> – The intermediate state of a transaction is invisible to other transactions. As a result, transactions that run concurrently appear to be serialized.</p>
<p>For example, in an application that transfers funds from one account to another, the isolation property ensures that another transaction sees the transferred funds in one account or the other, but not in both, nor in neither.</p>
<p><strong>Durability</strong> – After a transaction successfully completes, changes to data persist and are not undone, even in the event of a system failure.</p>
<p>For example, in an application that transfers funds from one account to another, the durability property ensures that the changes made to each account will not be reversed.</p>
<h3 id="banking-transaction-example">Banking transaction example</h3>
<p>Let’s look at an example from the <a href="https://www.postgresql.org/docs/8.3/tutorial-transactions.html">PostgreSQL Transactions doc</a> that demonstrates the ACID properties of a transaction. We have a bank database that contains customer account balances, as well as total deposit balances for branches. We want to record a payment of $100.00 from Alice’s account to Bob’s account, as well as update the total branch balances. The transaction would look like the code below.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb7-1" title="1"><span class="cf">BEGIN</span>;</a>
<a class="sourceLine" id="cb7-2" title="2">  <span class="kw">UPDATE</span> accounts <span class="kw">SET</span> balance <span class="op">=</span> balance <span class="op">-</span> <span class="fl">100.00</span></a>
<a class="sourceLine" id="cb7-3" title="3">      <span class="kw">WHERE</span> name <span class="op">=</span> <span class="st">&#39;Alice&#39;</span>;</a>
<a class="sourceLine" id="cb7-4" title="4">  <span class="kw">UPDATE</span> branches <span class="kw">SET</span> balance <span class="op">=</span> balance <span class="op">-</span> <span class="fl">100.00</span></a>
<a class="sourceLine" id="cb7-5" title="5">      <span class="kw">WHERE</span> name <span class="op">=</span> (<span class="kw">SELECT</span> branch_name <span class="kw">FROM</span> accounts <span class="kw">WHERE</span> name <span class="op">=</span> <span class="st">&#39;Alice&#39;</span>);</a>
<a class="sourceLine" id="cb7-6" title="6">  <span class="kw">UPDATE</span> accounts <span class="kw">SET</span> balance <span class="op">=</span> balance <span class="op">+</span> <span class="fl">100.00</span></a>
<a class="sourceLine" id="cb7-7" title="7">      <span class="kw">WHERE</span> name <span class="op">=</span> <span class="st">&#39;Bob&#39;</span>;</a>
<a class="sourceLine" id="cb7-8" title="8">  <span class="kw">UPDATE</span> branches <span class="kw">SET</span> balance <span class="op">=</span> balance <span class="op">+</span> <span class="fl">100.00</span></a>
<a class="sourceLine" id="cb7-9" title="9">      <span class="kw">WHERE</span> name <span class="op">=</span> (<span class="kw">SELECT</span> branch_name <span class="kw">FROM</span> accounts <span class="kw">WHERE</span> name <span class="op">=</span> <span class="st">&#39;Bob&#39;</span>);</a>
<a class="sourceLine" id="cb7-10" title="10"><span class="kw">COMMIT</span>;</a></code></pre></div>
<p>There are several updates happening above. The bank wants to make sure that all of the updates happen or none happen, in order to ensure that funds are transferred from the proper account (i.e. Alice’s account) to the proper recipient’s account (i.e. Bob’s account). If any of the updates fails, none of them will take effect. That is, if something goes wrong either with withdrawing funds from Alice’s account or transferring the funds into Bob’s account, then the entire transaction will be aborted and no changes will occur. This prevents Alice or Bob from seeing a transaction in their account summaries that isn’t supposed to be there.</p>
<p>There are many other scenarios where we would want to use an atomic operation to ensure a successful end result. Transactions are ideal for such scenarios, and we should use them whenever they’re applicable.</p>
<h2 id="helpful-links">Helpful links:</h2>
<ul>
<li><a href="https://www.postgresql.org/docs/8.3/tutorial-transactions.html">PostgreSQL: Transactions</a></li>
<li><a href="http://www.postgresqltutorial.com/postgresql-transaction/">PostgreSQL Tutorial: PostgreSQL Transaction</a></li>
<li><a href="https://www.postgresql.org/docs/9.1/sql-begin.html">PostgreSQL: BEGIN</a></li>
<li><a href="https://www.postgresql.org/docs/9.1/sql-commit.html">PostgreSQL: COMMIT</a></li>
<li><a href="https://www.postgresql.org/docs/9.1/sql-rollback.html">PostgreSQL: ROLLBACK</a></li>
<li><a href="https://www.postgresql.org/docs/9.1/sql-savepoint.html">PostgreSQL: SAVEPOINT</a></li>
<li><a href="https://www.postgresql.org/docs/9.1/sql-savepoint.html">PostgreSQL: SET TRANSACTION</a></li>
</ul>
