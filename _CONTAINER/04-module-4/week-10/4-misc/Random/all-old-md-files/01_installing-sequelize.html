<h1 id="installing-and-using-sequelize">Installing And Using Sequelize</h1>
<hr />
<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->
<ul>
<li><a href="#installing-and-using-sequelize">Installing And Using Sequelize</a>
<ul>
<li><a href="#what-is-an-orm">What Is An ORM?</a></li>
<li><a href="#how-to-install-sequelize">How To Install Sequelize</a></li>
<li><a href="#how-to-initialize-sequelize">How To Initialize Sequelize</a></li>
<li><a href="#verifying-that-sequelize-can-connect-to-the-database">Verifying That Sequelize Can Connect To The Database</a></li>
<li><a href="#our-preexisting-database-schema">Our Preexisting Database Schema</a></li>
<li><a href="#using-sequelize-to-generate-the-model-file">Using Sequelize To Generate The Model File</a></li>
<li><a href="#examining-and-modifying-a-sequelize-model-file">Examining (And Modifying) A Sequelize Model File</a></li>
<li><a href="#using-the-cat-model-to-fetch-and-update-sql-data">Using The <code>Cat</code> Model To Fetch And Update SQL Data</a></li>
<li><a href="#reading-and-changing-record-attributes">Reading And Changing Record Attributes</a></li>
<li><a href="#conclusion">Conclusion</a></li>
</ul></li>
</ul>
<!-- /code_chunk_output -->
<hr />
<p>Now that you have gained experience with SQL, it is time to learn how to access data stored in a SQL database using a JavaScript program. You will use a JavaScript library called Sequelize to do this. Sequelize is an example of an <em>Object Relational Mapping</em> (commonly abbreviated <em>ORM</em>). An ORM allows a JavaScript programmer to fetch and store data in a SQL database using JavaScript functions instead of writing SQL code.</p>
<p>When you finish this reading you will be able to:</p>
<ul>
<li>Describe what an Object Relational Mapping is and what it is used for.</li>
<li>Install and configure the packages needed to use Sequelize.</li>
<li>Use Sequelize to generate JavaScript code that fetches and stores data in a SQL database.</li>
<li>Use those auto-generated methods to fetch and store data in a SQL database.</li>
</ul>
<h2 id="what-is-an-orm">What Is An ORM?</h2>
<p>An <em>Object Relational Mapping</em> is a library that allows you to access data stored in a SQL database through object-oriented, non-SQL code (such as JavaScript). You will write <em>object-oriented</em> code that accesses data stored in a <em>relational</em> SQL database like Postgres. The ORM is the <em>mapping</em> that will “translate” your object-oriented code into SQL code that the database can run. The ORM will automatically generate SQL code for common tasks like fetching and storing data.</p>
<p>You will learn how to use the <a href="https://sequelize.org/v5/">Sequelize ORM</a>. Sequelize is the most widely used JavaScript ORM library.</p>
<h2 id="how-to-install-sequelize">How To Install Sequelize</h2>
<p>After creating a new node project with <code>npm init</code> we are ready to install the Sequelize library.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><a class="sourceLine" id="cb1-1" title="1"><span class="ex">npm</span> install sequelize@^5.0.0</a>
<a class="sourceLine" id="cb1-2" title="2"><span class="ex">npm</span> install sequelize-cli@^5.0.0</a>
<a class="sourceLine" id="cb1-3" title="3"><span class="ex">npm</span> install pg@^8.0.0</a></code></pre></div>
<p>We have installed not only the Sequelize library, but also a command line tool called <code>sequelize-cli</code> that will help us auto-generate and manage JavaScript files which will hold our Sequelize ORM code.</p>
<p>Last, we have also installed the pg library. This library allows Sequelize to access a Postgres database. If you were using a different database software (such as MySQL), you would need to install a different library.</p>
<h2 id="how-to-initialize-sequelize">How To Initialize Sequelize</h2>
<p>We can run the command <code>npx sequelize init</code> to automatically setup the following directory structure for our project:</p>
<pre><code>.
├── config
│   └── config.json
├── migrations
├── models
│   └── index.js
├── node_modules
├── package-lock.json
├── package.json
└── seeders</code></pre>
<blockquote>
<p>Aside: the <code>npx</code> tool allows you to easily run scripts provided by packages like <code>sequelize-cli</code>. If you don’t already have <code>npx</code>, you can install it with <code>npm install npx --global</code>. Without <code>npx</code> you would have to run the bash command: <code>./node_modules/.bin/sequelize init</code>. This directly runs the <code>sequelize</code> script provided by the installed <code>sequelize-cli</code> package.</p>
</blockquote>
<p>Having run <code>npx sequelize init</code>, we must write our database login information into <code>config/config.json</code>.</p>
<p>By default this file contains different sections we call “environments”. In a typical company you will have different database servers and configuration depending on where you app is running. Development is usually where you do your development work. In our case this is our local computer. But test might be and environment where you run tests, and production is the environment where real users are interacting with your application.</p>
<p>Since we are doing development, we can just modify the “development” section to look like this:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode json"><code class="sourceCode json"><a class="sourceLine" id="cb3-1" title="1"><span class="fu">{</span></a>
<a class="sourceLine" id="cb3-2" title="2">  <span class="dt">&quot;development&quot;</span><span class="fu">:</span> <span class="fu">{</span></a>
<a class="sourceLine" id="cb3-3" title="3">    <span class="dt">&quot;username&quot;</span><span class="fu">:</span> <span class="st">&quot;catsdbuser&quot;</span><span class="fu">,</span></a>
<a class="sourceLine" id="cb3-4" title="4">    <span class="dt">&quot;password&quot;</span><span class="fu">:</span> <span class="st">&quot;catsdbpassword&quot;</span><span class="fu">,</span></a>
<a class="sourceLine" id="cb3-5" title="5">    <span class="dt">&quot;database&quot;</span><span class="fu">:</span> <span class="st">&quot;catsdb&quot;</span><span class="fu">,</span></a>
<a class="sourceLine" id="cb3-6" title="6">    <span class="dt">&quot;host&quot;</span><span class="fu">:</span> <span class="st">&quot;127.0.0.1&quot;</span><span class="fu">,</span></a>
<a class="sourceLine" id="cb3-7" title="7">    <span class="dt">&quot;dialect&quot;</span><span class="fu">:</span> <span class="st">&quot;postgres&quot;</span></a>
<a class="sourceLine" id="cb3-8" title="8">  <span class="fu">}</span></a>
<a class="sourceLine" id="cb3-9" title="9"><span class="fu">}</span><span class="er">...</span></a></code></pre></div>
<p>Here we are supposing that we have already created a <code>catsdb</code> database owned by the user <code>catsdbuser</code>, with password <code>catsdbpassword</code>. By setting <code>host</code> to <code>127.0.0.1</code>, we are saying that the database will run on the same machine as my JavaScript application. Last, we specify that we are using a <code>postgres</code> database.</p>
<h2 id="verifying-that-sequelize-can-connect-to-the-database">Verifying That Sequelize Can Connect To The Database</h2>
<p>At the top level of our project, we should create an <code>index.js</code> file. From this file we will verify that Sequelize can connect to the SQL database. To do this, we use the <code>authenticate</code> method of the sequelize object.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode javascript"><code class="sourceCode javascript"><a class="sourceLine" id="cb4-1" title="1"><span class="co">// ./index.js</span></a>
<a class="sourceLine" id="cb4-2" title="2"></a>
<a class="sourceLine" id="cb4-3" title="3"><span class="kw">const</span> <span class="op">{</span> sequelize <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&quot;./models&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-4" title="4"></a>
<a class="sourceLine" id="cb4-5" title="5"><span class="kw">async</span> <span class="kw">function</span> <span class="at">main</span>() <span class="op">{</span></a>
<a class="sourceLine" id="cb4-6" title="6">  <span class="cf">try</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb4-7" title="7">    <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">authenticate</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb4-8" title="8">  <span class="op">}</span> <span class="cf">catch</span> (e) <span class="op">{</span></a>
<a class="sourceLine" id="cb4-9" title="9">    <span class="va">console</span>.<span class="at">log</span>(<span class="st">&quot;Database connection failure.&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-10" title="10">    <span class="va">console</span>.<span class="at">log</span>(e)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-11" title="11">    <span class="cf">return</span><span class="op">;</span></a>
<a class="sourceLine" id="cb4-12" title="12">  <span class="op">}</span></a>
<a class="sourceLine" id="cb4-13" title="13"></a>
<a class="sourceLine" id="cb4-14" title="14">  <span class="va">console</span>.<span class="at">log</span>(<span class="st">&quot;Database connection success!&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-15" title="15">  <span class="va">console</span>.<span class="at">log</span>(<span class="st">&quot;Sequelize is ready to use!&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-16" title="16"></a>
<a class="sourceLine" id="cb4-17" title="17">  <span class="co">// Close database connection when done with it.</span></a>
<a class="sourceLine" id="cb4-18" title="18">  <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">close</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb4-19" title="19"><span class="op">}</span></a>
<a class="sourceLine" id="cb4-20" title="20"></a>
<a class="sourceLine" id="cb4-21" title="21"><span class="at">main</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb4-22" title="22"></a>
<a class="sourceLine" id="cb4-23" title="23"><span class="co">// Prints:</span></a>
<a class="sourceLine" id="cb4-24" title="24"><span class="co">//</span></a>
<a class="sourceLine" id="cb4-25" title="25"><span class="co">// Executing (default): SELECT 1+1 AS result</span></a>
<a class="sourceLine" id="cb4-26" title="26"><span class="co">// Database connection success!</span></a>
<a class="sourceLine" id="cb4-27" title="27"><span class="co">// Sequelize is ready to use!</span></a></code></pre></div>
<p>You may observe that the <code>authenticate</code> method returns a JavaScript <code>Promise</code> object. We use <code>await</code> to wait for the database connection to be established. If <code>authenticate</code> fails to connect, the <code>Promise</code> will be rejected. Since we use <code>await</code>, an exception will be thrown.</p>
<p>Many Sequelize methods return <code>Promise</code>s. Using <code>async</code> and <code>await</code> lets us use Sequelize methods as if they were synchronous. This helps reduce code complexity significantly.</p>
<p>Note that I call <code>sequelize.close()</code>. This closes the connection to the database. A Node.js JavaScript program will not terminate until all open files and database connections are closed. Thus, to make sure the Node.js program doesn’t “hang” at the end, we close the database connection. Otherwise we will be forced to kill the Node.js program with <code>CTRL-C</code>, which is somewhat annoying.</p>
<h2 id="our-preexisting-database-schema">Our Preexisting Database Schema</h2>
<p>We are assuming that we are working with a preexisting SQL database. Our <code>catsdb</code> has a single table: <code>Cats</code>. Using the <code>psql</code> command-line program, we can describe the pre-existing <code>Cats</code> table below.</p>
<pre><code>catsdb=&gt; \d &quot;Cats&quot;
                                         Table &quot;public.Cats&quot;
    Column    |           Type           | Collation | Nullable |              Default
--------------+--------------------------+-----------+----------+------------------------------------
 id           | integer                  |           | not null | nextval(&#39;&quot;Cats_id_seq&quot;&#39;::regclass)
 firstName    | character varying(255)   |           |          |
 specialSkill | character varying(255)   |           |          |
 age          | integer                  |           |          |
 createdAt    | timestamp with time zone |           | not null |
 updatedAt    | timestamp with time zone |           | not null |
Indexes:
    &quot;Cats_pkey&quot; PRIMARY KEY, btree (id)</code></pre>
<p>Besides a primary key <code>id</code>, each <code>Cats</code> record has a <code>firstName</code>, a <code>specialSkill</code>, and an <code>age</code>. Each record also keeps track of two timestamps: the time when the cat was created (<code>createdAt</code>), and the most recent time when a column of the cat has been updated (<code>updatedAt</code>).</p>
<h2 id="using-sequelize-to-generate-the-model-file">Using Sequelize To Generate The Model File</h2>
<p>We will configure Sequelize to access the <code>Cats</code> table via a JavaScript class called <code>Cat</code>. To do this, we first use our trusty Sequelize CLI:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode bash"><code class="sourceCode bash"><a class="sourceLine" id="cb6-1" title="1"><span class="co"># Oops, forgot age:integer! (Don&#39;t worry we&#39;ll fix it later)</span></a>
<a class="sourceLine" id="cb6-2" title="2"><span class="ex">npx</span> sequelize model:generate --name Cat --attributes <span class="st">&quot;firstName:string,specialSkill:string&quot;</span></a></code></pre></div>
<p>This command generates two files: a <em>model</em> file (<code>./models/cat.js</code>) and a <em>migration</em> file (<code>./migrations/20200203211508-Cat.js</code>). We will ignore the migration file for now, and focus on the model file.</p>
<p>When using Sequelize’s <code>model:generate</code> command, we specify two things. First: we specify the <em>singular</em> form of the <code>Cats</code> table name (<code>Cat</code>). Second: we list the columns of the <code>Cats</code> table after the <code>--attributes</code> flag: <code>firstName</code> and <code>specialSkill</code>. We tell Sequelize that these are both <code>string</code> columns (Sequelize calls SQL <code>character varying(255)</code> columns <code>string</code>s).</p>
<p>We do not need to list <code>id</code>, <code>createdAt</code>, or <code>updatedAt</code>. Sequelize will always presume those exist. Notice that we have <strong>forgotten</strong> to list <code>age:integer</code> – we will fix that soon!</p>
<h2 id="examining-and-modifying-a-sequelize-model-file">Examining (And Modifying) A Sequelize Model File</h2>
<p>Let us examine the generated <code>./models/cat.js</code> file:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode javascript"><code class="sourceCode javascript"><a class="sourceLine" id="cb7-1" title="1"><span class="co">// ./models/cat.js</span></a>
<a class="sourceLine" id="cb7-2" title="2"></a>
<a class="sourceLine" id="cb7-3" title="3"><span class="st">&#39;use strict&#39;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb7-4" title="4"><span class="va">module</span>.<span class="at">exports</span> <span class="op">=</span> (sequelize<span class="op">,</span> DataTypes) <span class="kw">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb7-5" title="5">  <span class="kw">const</span> Cat <span class="op">=</span> <span class="va">sequelize</span>.<span class="at">define</span>(<span class="st">&#39;Cat&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb7-6" title="6">    <span class="dt">firstName</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb7-7" title="7">    <span class="dt">specialSkill</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span></a>
<a class="sourceLine" id="cb7-8" title="8">  <span class="op">},</span> <span class="op">{}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb7-9" title="9">  <span class="va">Cat</span>.<span class="at">associate</span> <span class="op">=</span> <span class="kw">function</span>(models) <span class="op">{</span></a>
<a class="sourceLine" id="cb7-10" title="10">    <span class="co">// associations can be defined here</span></a>
<a class="sourceLine" id="cb7-11" title="11">  <span class="op">};</span></a>
<a class="sourceLine" id="cb7-12" title="12">  <span class="cf">return</span> Cat<span class="op">;</span></a>
<a class="sourceLine" id="cb7-13" title="13"><span class="op">};</span></a></code></pre></div>
<p>This file exports a function that defines a <code>Cat</code> class. When you use <code>Sequelize</code> to query the <code>Cats</code> table, each row retrieved will be transformed by Sequelize into an instance of the <code>Cat</code> class. A JavaScript class like <code>Cat</code> that corresponds to a SQL table is called a <em>model</em> class.</p>
<p>The <code>./models/cat.js</code> will not be loaded by us directly. Sequelize will load this file and call the exported function to define the <code>Cat</code> class. The exported function uses Sequelize’s <code>define</code> method to auto-generate a new class (called <code>Cat</code>).</p>
<blockquote>
<p>Note: You may notice we aren’t using the JavaScript’s <code>class</code> keyword to define the Cat class. With Sequelize, it is going to do all that for us with the <code>define</code> method. This is because Sequelize was around way before the <code>class</code> keyword was added to JavaScript. It is possible to use the class keyword with Sequelize, but it’s <a href="https://codewithhugo.com/using-es6-classes-for-sequelize-4-models/">undocumented</a>.</p>
</blockquote>
<p>The first argument of <code>define</code> is the name of the class to define: <code>Cat</code>. Notice how the second argument is an <code>Object</code> of <code>Cats</code> table columns:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode javascript"><code class="sourceCode javascript"><a class="sourceLine" id="cb8-1" title="1"><span class="op">{</span></a>
<a class="sourceLine" id="cb8-2" title="2">    <span class="dt">firstName</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb8-3" title="3">    <span class="dt">specialSkill</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span></a>
<a class="sourceLine" id="cb8-4" title="4"><span class="op">}</span></a></code></pre></div>
<p>This object tells Sequelize about each of the columns of <code>Cats</code>. It maps each column name (<code>firstName</code>, <code>specialSkill</code>) to the type of data stored in the corresponding column of the <code>Cats</code> table. It is unnecessary to list <code>id</code>, <code>createdAt</code>, <code>updatedAt</code>, since Sequelize will already assume those exist.</p>
<p>We can correct our earlier mistake of forgetting <code>age</code>. We update the definition as so:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode javascript"><code class="sourceCode javascript"><a class="sourceLine" id="cb9-1" title="1"><span class="kw">const</span> Cat <span class="op">=</span> <span class="va">sequelize</span>.<span class="at">define</span>(<span class="st">&#39;Cat&#39;</span><span class="op">,</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb9-2" title="2">  <span class="dt">firstName</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb9-3" title="3">  <span class="dt">specialSkill</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">STRING</span><span class="op">,</span></a>
<a class="sourceLine" id="cb9-4" title="4">  <span class="dt">age</span><span class="op">:</span> <span class="va">DataTypes</span>.<span class="at">INTEGER</span><span class="op">,</span></a>
<a class="sourceLine" id="cb9-5" title="5"><span class="op">},</span> <span class="op">{}</span>)<span class="op">;</span></a></code></pre></div>
<p>A complete list of Sequelize datatypes can be found in the <a href="https://sequelize.org/v5/manual/data-types.html">documentation</a>.</p>
<h2 id="using-the-cat-model-to-fetch-and-update-sql-data">Using The <code>Cat</code> Model To Fetch And Update SQL Data</h2>
<p>We are now ready to use our <code>Cat</code> model class. When Sequelize defines the <code>Cat</code> class, it will generate instance and class methods needed to interact with the <code>Cats</code> SQL table.</p>
<p>As we mentioned before we don’t require our <code>cats.js</code> file directly. Instead we require <code>./models</code> which loads the file <code>./models/index.js</code>.</p>
<p>Inside this file it reads through all our models and attaches them to an object that it exports. So we can use destructuring to get a reference to our model class <code>Cat</code> like so:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode javascript"><code class="sourceCode javascript"><a class="sourceLine" id="cb10-1" title="1"><span class="kw">const</span> <span class="op">{</span> sequelize<span class="op">,</span> Cat <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&quot;./models&quot;</span>)<span class="op">;</span></a></code></pre></div>
<p>Now let’s update <em>our</em> <code>index.js</code> file to fetch a <code>Cat</code> from the <code>Cats</code> table:</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode javascript"><code class="sourceCode javascript"><a class="sourceLine" id="cb11-1" title="1"><span class="kw">const</span> <span class="op">{</span> sequelize <span class="op">,</span> Cat <span class="op">}</span> <span class="op">=</span> <span class="at">require</span>(<span class="st">&quot;./models&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb11-2" title="2"></a>
<a class="sourceLine" id="cb11-3" title="3"><span class="kw">async</span> <span class="kw">function</span> <span class="at">main</span>() <span class="op">{</span></a>
<a class="sourceLine" id="cb11-4" title="4">  <span class="cf">try</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb11-5" title="5">    <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">authenticate</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb11-6" title="6">  <span class="op">}</span> <span class="cf">catch</span> (e) <span class="op">{</span></a>
<a class="sourceLine" id="cb11-7" title="7">    <span class="va">console</span>.<span class="at">log</span>(<span class="st">&quot;Database connection failure.&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb11-8" title="8">    <span class="va">console</span>.<span class="at">log</span>(e)<span class="op">;</span></a>
<a class="sourceLine" id="cb11-9" title="9">    <span class="cf">return</span><span class="op">;</span></a>
<a class="sourceLine" id="cb11-10" title="10">  <span class="op">}</span></a>
<a class="sourceLine" id="cb11-11" title="11"></a>
<a class="sourceLine" id="cb11-12" title="12">  <span class="va">console</span>.<span class="at">log</span>(<span class="st">&quot;Database connection success!&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb11-13" title="13">  <span class="va">console</span>.<span class="at">log</span>(<span class="st">&quot;Sequelize is ready to use!&quot;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb11-14" title="14"></a>
<a class="sourceLine" id="cb11-15" title="15">  <span class="kw">const</span> cat <span class="op">=</span> <span class="cf">await</span> <span class="va">Cat</span>.<span class="at">findByPk</span>(<span class="dv">1</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb11-16" title="16">  <span class="va">console</span>.<span class="at">log</span>(<span class="va">cat</span>.<span class="at">toJSON</span>())<span class="op">;</span></a>
<a class="sourceLine" id="cb11-17" title="17"></a>
<a class="sourceLine" id="cb11-18" title="18">  <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">close</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb11-19" title="19"><span class="op">}</span></a>
<a class="sourceLine" id="cb11-20" title="20"></a>
<a class="sourceLine" id="cb11-21" title="21"><span class="at">main</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb11-22" title="22"></a>
<a class="sourceLine" id="cb11-23" title="23"><span class="co">// This code prints:</span></a>
<a class="sourceLine" id="cb11-24" title="24"><span class="co">//</span></a>
<a class="sourceLine" id="cb11-25" title="25"><span class="co">// Executing (default): SELECT &quot;id&quot;, &quot;firstName&quot;, &quot;specialSkill&quot;, &quot;age&quot;, &quot;createdAt&quot;, &quot;updatedAt&quot; FROM &quot;Cats&quot; AS &quot;Cat&quot; WHERE &quot;Cat&quot;.&quot;id&quot; = 1;</span></a>
<a class="sourceLine" id="cb11-26" title="26"><span class="co">// {</span></a>
<a class="sourceLine" id="cb11-27" title="27"><span class="co">//   id: 1,</span></a>
<a class="sourceLine" id="cb11-28" title="28"><span class="co">//   firstName: &#39;Markov&#39;,</span></a>
<a class="sourceLine" id="cb11-29" title="29"><span class="co">//   specialSkill: &#39;sleeping&#39;,</span></a>
<a class="sourceLine" id="cb11-30" title="30"><span class="co">//   age: 5,</span></a>
<a class="sourceLine" id="cb11-31" title="31"><span class="co">//   createdAt: 2020-02-03T21:32:28.960Z,</span></a>
<a class="sourceLine" id="cb11-32" title="32"><span class="co">//   updatedAt: 2020-02-03T21:32:28.960Z</span></a>
<a class="sourceLine" id="cb11-33" title="33"><span class="co">// }</span></a></code></pre></div>
<p>We use the <code>Cat.findByPk</code> static class method to fetch a single cat: the one with <code>id</code> equal to 1. This static method exists because our <code>Cat</code> model class <em>extends</em> <code>Sequelize.Model</code>.</p>
<p>“Pk” stands for <em>primary key</em>; the <code>id</code> field is the primary key for the <code>Cats</code> table. <code>findByPk</code> returns a <code>Promise</code>, so we must <code>await</code> the result. The result is an instance of the <code>Cat</code> model class.</p>
<p>The cleanest way to log a fetched database record is to first call the <code>toJSON</code> method. <code>toJSON</code> converts a <code>Cat</code> object to a <em>Plain Old JavaScript Object</em> (POJO). <code>Cat</code> instances have many private variables and methods that can be distracting when printed. When you call <code>toJSON</code>, only the public data fields are copied into a JavaScript <code>Object</code>. Printing this raw JavaScript <code>Object</code> is thus much cleaner.</p>
<blockquote>
<p>The author has a pet-peeve about the <code>.toJSON()</code> method of Sequelize, it does not return JSON. It instead returns a POJO. If you needed it to be JSON you would still need to call <code>JSON.stringify(cat.toJSON())</code>. Perhaps they should have called it <code>.toObject</code> or <code>.toPOJO</code> instead.</p>
</blockquote>
<p>Note that Sequelize has logged the SQL query it ran to fetch Markov’s information. This logging information is often helpful when trying to figure out what Sequelize is doing.</p>
<p>You’ll also notice that Sequelize puts double quotes around the table and field names. So if you are trying to look at your “Cats” table from the <code>psql</code> command you will need to quote them there as well. This is because PostgreSQL lowercases all identifiers like table and fields names before the query is run if they aren’t quoted.</p>
<h2 id="reading-and-changing-record-attributes">Reading And Changing Record Attributes</h2>
<p>While <code>toJSON</code> is useful for logging a <code>Cat</code> object, it is not the simplest way to access individual column values. To read the <code>id</code>, <code>firstName</code>, etc of a <code>Cat</code>, you can directly access those attributes on the <code>Cat</code> instance itself:</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode javascript"><code class="sourceCode javascript"><a class="sourceLine" id="cb12-1" title="1"><span class="kw">async</span> <span class="kw">function</span> <span class="at">main</span>() <span class="op">{</span></a>
<a class="sourceLine" id="cb12-2" title="2">  <span class="co">// Sequelize authentication code from above...</span></a>
<a class="sourceLine" id="cb12-3" title="3"></a>
<a class="sourceLine" id="cb12-4" title="4">  <span class="kw">const</span> cat <span class="op">=</span> <span class="cf">await</span> <span class="va">Cat</span>.<span class="at">findByPk</span>(<span class="dv">1</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb12-5" title="5">  <span class="va">console</span>.<span class="at">log</span>(<span class="vs">`</span><span class="sc">${</span><span class="va">cat</span>.<span class="at">firstName</span><span class="sc">}</span><span class="vs"> has been assigned id #</span><span class="sc">${</span><span class="va">cat</span>.<span class="at">id</span><span class="sc">}</span><span class="vs">.`</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb12-6" title="6">  <span class="va">console</span>.<span class="at">log</span>(<span class="vs">`They are </span><span class="sc">${</span><span class="va">cat</span>.<span class="at">age</span><span class="sc">}</span><span class="vs"> years old.`</span>)</a>
<a class="sourceLine" id="cb12-7" title="7">  <span class="va">console</span>.<span class="at">log</span>(<span class="vs">`Their special skill is </span><span class="sc">${</span><span class="va">cat</span>.<span class="at">specialSkill</span><span class="sc">}</span><span class="vs">.`</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb12-8" title="8"></a>
<a class="sourceLine" id="cb12-9" title="9">  <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">close</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb12-10" title="10"><span class="op">}</span></a>
<a class="sourceLine" id="cb12-11" title="11"></a>
<a class="sourceLine" id="cb12-12" title="12"><span class="at">main</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb12-13" title="13"></a>
<a class="sourceLine" id="cb12-14" title="14"><span class="co">// This code prints:</span></a>
<a class="sourceLine" id="cb12-15" title="15"><span class="co">//</span></a>
<a class="sourceLine" id="cb12-16" title="16"><span class="co">// Executing (default): SELECT &quot;id&quot;, &quot;firstName&quot;, &quot;specialSkill&quot;, &quot;age&quot;, &quot;createdAt&quot;, &quot;updatedAt&quot; FROM &quot;Cats&quot; AS &quot;Cat&quot; WHERE &quot;Cat&quot;.&quot;id&quot; = 1;</span></a>
<a class="sourceLine" id="cb12-17" title="17"><span class="co">// Markov has been assigned id #1.</span></a>
<a class="sourceLine" id="cb12-18" title="18"><span class="co">// They are 5 years old.</span></a>
<a class="sourceLine" id="cb12-19" title="19"><span class="co">// Their special skill is sleeping.</span></a></code></pre></div>
<p>Accessing data directly through the <code>Cat</code> object is just like reading an attribute on any other JavaScript class. You may likewise <em>change</em> values in the database:</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode javascript"><code class="sourceCode javascript"><a class="sourceLine" id="cb13-1" title="1"><span class="kw">async</span> <span class="kw">function</span> <span class="at">main</span>() <span class="op">{</span></a>
<a class="sourceLine" id="cb13-2" title="2">  <span class="co">// Sequelize authentication code from above...</span></a>
<a class="sourceLine" id="cb13-3" title="3"></a>
<a class="sourceLine" id="cb13-4" title="4">  <span class="co">// Fetch existing cat from database.</span></a>
<a class="sourceLine" id="cb13-5" title="5">  <span class="kw">const</span> cat <span class="op">=</span> <span class="cf">await</span> <span class="va">Cat</span>.<span class="at">findByPk</span>(<span class="dv">1</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb13-6" title="6">  <span class="co">// Change cat&#39;s attributes.</span></a>
<a class="sourceLine" id="cb13-7" title="7">  <span class="va">cat</span>.<span class="at">firstName</span> <span class="op">=</span> <span class="st">&quot;Curie&quot;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb13-8" title="8">  <span class="va">cat</span>.<span class="at">specialSkill</span> <span class="op">=</span> <span class="st">&quot;jumping&quot;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb13-9" title="9">  <span class="va">cat</span>.<span class="at">age</span> <span class="op">=</span> <span class="dv">123</span><span class="op">;</span></a>
<a class="sourceLine" id="cb13-10" title="10"></a>
<a class="sourceLine" id="cb13-11" title="11">  <span class="co">// Save the new name to the database.</span></a>
<a class="sourceLine" id="cb13-12" title="12">  <span class="cf">await</span> <span class="va">cat</span>.<span class="at">save</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb13-13" title="13"></a>
<a class="sourceLine" id="cb13-14" title="14">  <span class="cf">await</span> <span class="va">sequelize</span>.<span class="at">close</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb13-15" title="15"><span class="op">}</span></a>
<a class="sourceLine" id="cb13-16" title="16"></a>
<a class="sourceLine" id="cb13-17" title="17"><span class="co">// Prints:</span></a>
<a class="sourceLine" id="cb13-18" title="18"><span class="co">//</span></a>
<a class="sourceLine" id="cb13-19" title="19"><span class="co">// Executing (default): SELECT &quot;id&quot;, &quot;firstName&quot;, &quot;specialSkill&quot;, &quot;age&quot;, &quot;createdAt&quot;, &quot;updatedAt&quot; FROM &quot;Cats&quot; AS &quot;Cat&quot; WHERE &quot;Cat&quot;.&quot;id&quot; = 1;</span></a>
<a class="sourceLine" id="cb13-20" title="20"><span class="co">// Executing (default): UPDATE &quot;Cats&quot; SET &quot;firstName&quot;=$1,&quot;specialSkill&quot;=$2,&quot;age&quot;=$3,&quot;updatedAt&quot;=$4 WHERE &quot;id&quot; = $5</span></a>
<a class="sourceLine" id="cb13-21" title="21"></a>
<a class="sourceLine" id="cb13-22" title="22"><span class="at">main</span>()<span class="op">;</span></a></code></pre></div>
<p>Note that changing the <code>firstName</code> attribute value does not immediately change the stored value in the SQL database. Changing the <code>firstName</code> without calling <code>save</code> <strong>has no effect</strong> on the database. Only when we call <code>cat.save()</code> (and <code>await</code> the promise to resolve) will the changes to <code>firstName</code>, <code>specialSkill</code>, and <code>age</code> be saved to the SQL database. All these values are updated simultaneously.</p>
<h2 id="conclusion">Conclusion</h2>
<p>Having completed this reading, you should be able to:</p>
<ul>
<li>Describe what an Object Relational Mapping is and what it is used for.</li>
<li>Install the <code>sequelize</code>, <code>sequelize-cli</code>, <code>pg</code> packages.</li>
<li>Configure Sequelize via the <code>config/config.json</code> file.</li>
<li>Use Sequelize’s <code>authenticate</code> method to verify that Sequelize can connect to the database.</li>
<li>Use the Sequelize CLI <code>model:generate</code> command to generate a model file.</li>
<li>Configure a model file to tell Sequelize about each database column.</li>
<li>Use the <code>findByPk</code> class method to fetch data from a SQL table.</li>
<li>Read data attributes from a model instance.</li>
<li>Modify a model instance’s attributes and save the changes back to the SQL database using the <code>save</code> method.</li>
</ul>
