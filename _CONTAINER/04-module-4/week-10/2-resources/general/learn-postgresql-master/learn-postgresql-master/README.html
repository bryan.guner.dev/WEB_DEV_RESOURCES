<h2 id="learn-postgresql">learn-postgresql</h2>
<p>Learn how to use PostgreSQL to store your relational data</p>
<h3 id="installation">Installation</h3>
<p>Before you get started with using PostgreSQL, you’ll have to install it. Follow these steps to get started:</p>
<h4 id="macos">MacOS</h4>
<ol type="1">
<li><p>There are a couple of ways to install PostgreSQL. One of the easier ways to get started is with Postgres.app. Navigate to http://postgresapp.com/ and then click “Download”: <img src="https://cloud.githubusercontent.com/assets/12450298/19641848/6d3cfa4a-99da-11e6-858f-3ff2ada026be.png" alt="download" /></p></li>
<li><p>Once it’s finished downloading, double click on the file to unzip then move the PostgreSQL elephant icon into your <code>applications</code> folder. Double click the icon to launch the application.</p></li>
<li><p>You should see a new window launched that says “Welcome to Postgres”. If it says that it cannot connect to the postgres server this means that the DEFAULT port is probably already in use. Make sure you don’t have any other instances of Postgres on your computer. Uninstall them if you do and then resume with these steps. Click on the button that says “Open psql”: <img src="https://cloud.githubusercontent.com/assets/12450298/19642044/463eceae-99db-11e6-8907-bb3a6cc532a7.png" alt="open psql" /></p></li>
<li><p>Postgres.app will by default create a role and database that matches your current macOS username. You can connect straight away by running <code>psql</code>.</p></li>
<li><p>You should then see something in your terminal that looks like this (with your macOS username in front of the prompt rather than ‘postgres’): <img src="https://cloud.githubusercontent.com/assets/12450298/19642816/f8ac0c66-99de-11e6-87e2-db55e6abc27b.png" alt="terminal" /></p></li>
<li><p>You should now be all set up to start using PostgreSQL. For documentation on command line tools etc see http://postgresapp.com/documentation/</p></li>
</ol>
<h4 id="ubuntu">Ubuntu</h4>
<p>Digital Ocean have got a great article on <a href="https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04">getting started with postgres</a>. A quick summary is below.</p>
<h5 id="installation-1">Installation</h5>
<pre><code>sudo apt-get update
sudo apt-get install postgresql postgresql-contrib</code></pre>
<p>By default the only role created is the default ‘postgres’, so PostgreSQL will only respond to connections from an Ubuntu user called ‘postgres’. We need to pretend to be that user and create a role matching our actual Ubuntu username:</p>
<pre><code>sudo -u postgres createuser --interactive</code></pre>
<p>This command means ‘run the command <code>createuser --interactive</code> as the user called “postgres”’.</p>
<p>When asked for the name of the role enter your Ubuntu username. If you’re not sure, open a new Terminal tab and run <code>whoami</code>.</p>
<p>When asked if you want to make the role a superuser, type ‘y’.</p>
<p>We now need to create the database matching the role name, as PostgreSQL expects this. Run:</p>
<pre><code>sudo -u postgres createdb [your user name]</code></pre>
<p>You can now connect to PostgreSQL by running <code>psql</code>.</p>
<h3 id="create-your-first-postgresql-database">Create your first PostgreSQL database</h3>
<ol type="1">
<li><p>To start PostgreSQL, type this command into the terminal:<br />
<code>psql</code></p></li>
<li><p>Next type this command into the PostgreSQL interface:<br />
<code>CREATE DATABASE test;</code><br />
<strong>NOTE:</strong> Don’t forget the semi-colon. If you do, useful error messages won’t show up.</p></li>
<li><p>To check that our database has been created, type <code>\l</code> into the psql prompt. You should see something like this in your terminal: <img src="https://cloud.githubusercontent.com/assets/12450298/19650613/ce278678-9a01-11e6-89ad-b124c0adcfe5.png" alt="test db" /></p></li>
</ol>
<h3 id="create-new-users-for-your-database">Create new users for your database</h3>
<ol type="1">
<li><p>If you closed the PostgreSQL server, start it again with:<br />
<code>psql</code></p></li>
<li><p>To create a new user, type the following into the psql prompt:<br />
<code>sql  CREATE USER testuser;</code></p></li>
<li><p>Check that your user has been created. Type <code>\du</code> into the prompt. You should see something like this: <img src="https://cloud.githubusercontent.com/assets/12450298/19650852/9c340708-9a02-11e6-8f06-75f1e30a86b3.png" alt="user" /> Users can be given certain permissions to access any given database you have created.</p></li>
<li><p>Next we need to give our user permissions to access the test database we created above. Enter the following command into the <code>psql</code> prompt:<br />
<code>sql  GRANT ALL PRIVILEGES ON DATABASE test TO testuser;</code></p></li>
</ol>
<h3 id="postgis---spacial-and-geographic-objects-for-postgresql">PostGIS - Spacial and Geographic objects for PostgreSQL</h3>
<h4 id="postgis-installation">PostGIS Installation</h4>
<p>If you’ve installed Postgres App as in the example above, you can easily extend it to include PostGIS. Follow these steps to begin using PostGIS:</p>
<ol type="1">
<li><p>Ensure that you’re logged in as a user OTHER THAN <code>postgres</code>. Follow the steps above to enable your default user to be able to access the <code>psql</code> prompt. (<em><a href="#installation">installation step 7</a></em>)</p></li>
<li><p>Type the following into the <code>psql</code> prompt to add the extension:<br />
<code>CREATE EXTENSION postgis;</code></p></li>
</ol>
<h4 id="postgis-distance-between-two-sets-of-coordinates">PostGIS Distance between two sets of coordinates</h4>
<p>After you’ve extended PostgreSQL with PostGIS you can begin to use it. Type the following command into the <code>psql</code> command line:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb4-1" title="1"><span class="kw">SELECT</span> ST_Distance(gg1, gg2) <span class="kw">As</span> spheroid_dist</a>
<a class="sourceLine" id="cb4-2" title="2"><span class="kw">FROM</span> (<span class="kw">SELECT</span></a>
<a class="sourceLine" id="cb4-3" title="3">    ST_GeogFromText(<span class="st">&#39;SRID=4326;POINT(-72.1235 42.3521)&#39;</span>) <span class="kw">As</span> gg1,</a>
<a class="sourceLine" id="cb4-4" title="4">    ST_GeogFromText(<span class="st">&#39;SRID=4326;POINT(-72.1235 43.1111)&#39;</span>) <span class="kw">As</span> gg2</a>
<a class="sourceLine" id="cb4-5" title="5">    ) <span class="kw">As</span> foo  ;</a></code></pre></div>
<p>This should return <code>spheroid_dist</code> along with a value in meters. The example above returns: <code>84315.42034614</code> which is rougly 84.3km between the two points.</p>
<h3 id="commands">Commands</h3>
<p>Once you are serving the database from your computer</p>
<ul>
<li><p>To change db <code>\connect database_name;</code></p></li>
<li><p>To see the tables in the database <code>\d;</code></p></li>
<li><p>To select (and show in terminal) all tables <code>SELECT * FROM table_name</code></p></li>
<li><p>To make a table <code>CREATE TABLE table_name (col_name1, col_name2)</code></p></li>
<li><p>To add a row <code>INSERT INTO table_name ( col_name ) VALUES ( col_value)</code> col_name only require if only some of the cols are being filled out</p></li>
<li><p>To edit a column to a table  <code>ALTER TABLE table_name   ALTER COLUMN column_name SET DEFAULT expression</code></p></li>
<li><p>To add a column to a table  <code>ALTER TABLE table_name   ADD COLUMN column_name data_type</code></p></li>
<li><p>To find the number of instances where the word “Day” is present in the title of a table <code>SELECT count(title) FROM table_name WHERE title LIKE '%Day%’;</code></p></li>
<li><p>To delete a row in a table <code>DELETE FROM table_name WHERE column_name = ‘hello';</code></p></li>
</ul>
<p>Postgresql follows the SQL convention of calling relations TABLES, attributes COLUMNs and tuples ROWS</p>
<p><strong>Transaction</strong> All or nothing, if something fails the other commands are rolled back like nothing happened</p>
<p><strong>Reference</strong> When a table is being created you can reference a column in another table to make sure any value which is added to that column exists in the referenced table.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb5-1" title="1"><span class="kw">CREATE</span> <span class="kw">TABLE</span> cities (</a>
<a class="sourceLine" id="cb5-2" title="2">  name text <span class="kw">NOT</span> <span class="kw">NULL</span>,</a>
<a class="sourceLine" id="cb5-3" title="3">  postal_code <span class="dt">varchar</span>(<span class="dv">9</span>) <span class="kw">CHECK</span> (postal_code <span class="op">&lt;&gt;</span> <span class="st">&#39;&#39;</span>),</a>
<a class="sourceLine" id="cb5-4" title="4">  country_code <span class="dt">char</span>(<span class="dv">2</span>) <span class="kw">REFERENCES</span> countries,</a>
<a class="sourceLine" id="cb5-5" title="5">  <span class="kw">PRIMARY</span> <span class="kw">KEY</span> (country_code, postal_code)</a>
<a class="sourceLine" id="cb5-6" title="6">);</a></code></pre></div>
<p><code>&lt;&gt;</code> means not equal</p>
<p><strong>Join reads</strong> You can join tables together when reading them,</p>
<p><strong>Inner Join</strong> Joins together two tables by specifying a column in each to join them by i.e.</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb6-1" title="1"><span class="kw">SELECT</span> cities.<span class="op">*</span>, country_name</a>
<a class="sourceLine" id="cb6-2" title="2">  <span class="kw">FROM</span> cities <span class="kw">INNER</span> <span class="kw">JOIN</span> countries</a>
<a class="sourceLine" id="cb6-3" title="3">  <span class="kw">ON</span> cities.country_code <span class="op">=</span> countries.country_code;</a></code></pre></div>
<p>This will select all of the columns in both the countries and cities tables the data, the rows are matched up by country_code.</p>
<p><strong>Grouping</strong> You can put rows into groups where the group is defined by a shared value in a particular column.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb7-1" title="1"><span class="kw">SELECT</span> venue_id, <span class="fu">count</span>(<span class="op">*</span>)</a>
<a class="sourceLine" id="cb7-2" title="2">  <span class="kw">FROM</span> <span class="kw">events</span></a>
<a class="sourceLine" id="cb7-3" title="3">  <span class="kw">GROUP</span> <span class="kw">BY</span> venue_id;</a></code></pre></div>
<p>This will group the rows together by the venue_id, count is then performed on each of the groups.</p>
<h3 id="learning-resources">Learning Resources</h3>
<ul>
<li><a href="https://blog.risingstack.com/node-js-database-tutorial/">Node-hero</a></li>
<li><a href="https://www.pluralsight.com/courses/postgresql-getting-started">Pluralsight</a></li>
<li><a href="http://www.techrepublic.com/blog/diy-it-guy/diy-a-postgresql-database-server-setup-anyone-can-handle/">Tech Republic</a></li>
<li><a href="http://postgis.net/install/">PostGIS install</a></li>
<li><a href="http://postgis.net/docs/manual-2.3/">PostGIS docs</a></li>
<li><a href="http://postgis.net/docs/ST_Distance.html">PostGIS ST_Distance</a></li>
</ul>
