<p><strong>This is a walk-through</strong>: Please type along as you read what’s going on in this article.</p>
<p>In this walk-through, you will</p>
<ul>
<li>Learn about what a table is,</li>
<li>How to create and delete tables,</li>
<li>Who owns a table, and,</li>
<li>Learn about different data types that you can use when defining a table.</li>
</ul>
<p>You can now create users that can connect to the relational database management system. You can now create databases and secure them so only certain users can connect to them. Now, it’s time to get into the part where you define the entities that actually hold the data: <strong>tables</strong>!</p>
<h2 id="what-is-a-table">What is a table?</h2>
<p>A table is a “logical” structure that defines how data is stored and contains that data that meets the definition. Most people think about tables like spreadsheets that have a specific number of columns and rows that contain the data.</p>
<p>It is called a “logical” structure because we reason about it in terms of columns and rows; however, the RDMBS is in charge of how the data is actually stored on disk and, quite often, for performance reasons, it does <em>not</em> look like rows and columns. The way it is stored on disk is called the “physical” structure because that’s what is the actual physical representation of it. We do not cover physical structures because they vary by RDBMS. If you become a <strong>database administrator</strong> in the future, you may have to learn such things.</p>
<p>Here is a spreadsheet that contains some data about puppies.</p>
<figure>
<img src="https://appacademy-open-assets.s3-us-west-1.amazonaws.com/Module-SQL/assets/tables-puppies-spreadsheet.png" alt="Puppies spreadsheet" /><figcaption>Puppies spreadsheet</figcaption>
</figure>
<p>You can see that the columns are</p>
<ul>
<li>name</li>
<li>age_yrs</li>
<li>breed</li>
<li>weight_lbs</li>
<li>microchipped</li>
</ul>
<p>Now, look at the data each column contains. You can guess at what kind of data type is in each of them by their values. If you were to write that out using the data types that you know from JavaScript, you might come up with the following table.</p>
<table>
<thead>
<tr class="header">
<th>Column</th>
<th>Data type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>name</td>
<td>string</td>
</tr>
<tr class="even">
<td>age_yrs</td>
<td>number</td>
</tr>
<tr class="odd">
<td>breed</td>
<td>string</td>
</tr>
<tr class="even">
<td>weight_lbs</td>
<td>number</td>
</tr>
<tr class="odd">
<td>microchipped</td>
<td>Boolean</td>
</tr>
</tbody>
</table>
<p>In table definitions, you have to be more specific, unfortunately. This is so the database can know things like “the maximum length of the string” or “will the number have decimal points”? This is important information so that database can know how to store it most efficiently. The following table shows you the corresponding ANSI SQL data types for the JavaScript types from before.</p>
<table>
<thead>
<tr class="header">
<th>Column</th>
<th>JavaScript data type</th>
<th>Max length</th>
<th>ANSI SQL data type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>name</td>
<td>string</td>
<td>50</td>
<td>VARCHAR(50)</td>
</tr>
<tr class="even">
<td>age_yrs</td>
<td>number</td>
<td></td>
<td>NUMERIC(3,1)</td>
</tr>
<tr class="odd">
<td>breed</td>
<td>string</td>
<td>100</td>
<td>VARCHAR(100)</td>
</tr>
<tr class="even">
<td>weight_lbs</td>
<td>number</td>
<td></td>
<td>INTEGER</td>
</tr>
<tr class="odd">
<td>microchipped</td>
<td>Boolean</td>
<td></td>
<td>BOOLEAN</td>
</tr>
</tbody>
</table>
<p>You can see that <strong>“string” values map to something called a “VARCHAR”</strong> with a maximum length.</p>
<p>You can see that “number” values map to something called a “NUMERIC” with some numbers, or an INTEGER which is just a whole number.</p>
<p>You can see that “Boolean” values map to something called a “BOOLEAN” which is nice because that’s convenient.</p>
<h2 id="defining-tables">Defining tables</h2>
<p>To define a table, you need to know what the different pieces of related data it will store. Then, you need to know what kind each of those pieces are. Once you have that, you can create a table with an ANSI SQL statement.</p>
<h3 id="string-types">String types</h3>
<p>There are three kinds of commonly used string types that databases support based on the ANSI SQL standard. This section talks about them.</p>
<p>The most commonly used type is known as the <strong>CHARACTER VARYING</strong> type. It means that it can <em>contain text of varying lengths up to a specified maximum</em>. That maximum is provided when you define the table.</p>
<p>Instead of having to type <em>CHARACTER VARYING</em> all the time, you can use its weirdly named alias <strong>VARCHAR</strong>, . So, to specify that a column can hold up to 50 characters, you would write <code>VARCHAR(50)</code> in the table definition. (Remember, <strong>SQL is case insensitive for its keywords</strong>.</p>
<p>You can also write <code>varchar(50)</code> or <code>VarChar(50)</code> if you so desired. Just be consistent.)</p>
<p>Another commonly used type is known simply as <strong>TEXT</strong>.</p>
<p>This is a column that can contain an “unlimited” number of characters. You may ask yourself, “Why don’t I just always use TEXT, then?” Performance is the reason. Columns with the <em>TEXT</em> type are notoriously slower than those with other string types.</p>
<h3 id="numeric-types">Numeric types</h3>
<p>ANSI SQL (and PostgreSQL) supports <strong>INTEGER</strong>, <strong>NUMERIC</strong>, and <strong>floating-point numbers.</strong></p>
<p>The <em>INTEGER</em> should be familiar. It’s just a number. In PostgreSQL, it can hold almost all values that your application can handle. That’s from -2,147,483,648 to +2,147,483,647.</p>
<p>ANSI SQL (and PostgreSQL) supports the <strong>BIGINT</strong> type that will hold values between -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807. If your application needs bigger integers, there are extensions available.</p>
<p>The <strong>NUMERIC</strong> type is a fixed-point number. When you specify it, it takes up to two arguments.</p>
<pre><code>1. The first number is the total number of digits that a number can have in that column. 
2. The second number is the number of digits after the decimal point that you want to track. </code></pre>
<p>The specification <em>NUMERIC(4,2)</em> will hold the number <em>23.22</em>, but not the numbers <em>123.22</em> (too many total digits) or <em>23.222</em> (which it will just ignore the extra decimal places and store <em>23.22</em>).</p>
<p>These exact numbers are important for things like storing values of money, where rounding errors could cause significant errors in calculations.</p>
<p>If you don’t care about rounding errors, you can use the <strong>DOUBLE PRECISION</strong>. There is no short alias for it.</p>
<p>You can just put decimal numbers in there and they will come out pretty much the same.</p>
<p><strong>Don’t use this kind of data type for columns that contain values of money because they will round</strong> and someone will get in trouble, eventually.</p>
<h2 id="other-data-types">Other data types</h2>
<p>PostgreSQL supports a lot of other data types, as well. It has specialized data types for money, dates and times, geometric types, network addresses, and JSON! Ones that you will use a lot in this course are the ones for dates and times, as well as the one for JSON.</p>
<p>Here’s the link to the documentation on <a href="https://www.postgresql.org/docs/current/datatype.html">PostgreSQL data types</a>.</p>
<p>Go review the documentation for the types that support dates and times as you will need to know the <strong>TIMESTAMP</strong> and <strong>DATE</strong> types.</p>
<h2 id="naming-a-table">Naming a table</h2>
<p><strong>Names of tables should not create spaces or dashes</strong>.</p>
<ul>
<li>They should contain only <strong>lower case letters, numbers, and underscores</strong>.</li>
</ul>
<p>Conventionally, many software developers name their database table names as the plural form of the data that it holds.</p>
<p>More importantly, many software libraries known as ORMs (which you will cover, this week) use the plural naming convention. You should use the plural naming convention while here at App Academy.</p>
<ul>
<li>Good table names
<ul>
<li>student_grades</li>
<li>office_locations</li>
<li>people</li>
</ul></li>
<li>Bad (incorrect) table names
<ul>
<li>Student Grades</li>
<li>office-locations</li>
<li>person</li>
</ul></li>
</ul>
<h2 id="writing-the-sql">Writing the SQL</h2>
<p>Creating a table with SQL has this general syntax. ```sql CREATE TABLE «table name» ( «column name» «data type», «column name» «data type», … «column name» «data type» );</p>
<p>A couple of things to note. First, it uses parentheses, not curly braces. Many developers that use curly brace languages like JavaScript will eventually, out of habit, put curly braces instead of parentheses. If you were to do that, the RDBMS will tell you that you have a syntax error. Just grin and replace the curly braces with parentheses.</p>
<p>Another thing to note is that the last column specification <em>cannot</em> have a comma after it. In JavaScript, we can have commas after the last entry in an array or in a literal object definition. Not so in SQL. Again, the RDBMS will tell you that there is a syntax error. Just delete that last comma.</p>
<p>Here’s the table that contains the column definitions for the “puppies” spreadsheet from before.</p>
<table>
<thead>
<tr class="header">
<th>Column</th>
<th>JavaScript data type</th>
<th>Max length</th>
<th>ANSI SQL data type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>name</td>
<td>string</td>
<td>50</td>
<td>VARCHAR(50)</td>
</tr>
<tr class="even">
<td>age_yrs</td>
<td>number</td>
<td></td>
<td>NUMERIC(3,1)</td>
</tr>
<tr class="odd">
<td>breed</td>
<td>string</td>
<td>100</td>
<td>VARCHAR(100)</td>
</tr>
<tr class="even">
<td>weight_lbs</td>
<td>number</td>
<td></td>
<td>INTEGER</td>
</tr>
<tr class="odd">
<td>microchipped</td>
<td>Boolean</td>
<td></td>
<td>BOOLEAN</td>
</tr>
</tbody>
</table>
<p>To write that as SQL, you would just put in the table name, column names, and data types in the syntax from above. You would get the following.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sql"><code class="sourceCode sql"><a class="sourceLine" id="cb2-1" title="1"></a>
<a class="sourceLine" id="cb2-2" title="2">    <span class="kw">CREATE</span> <span class="kw">TABLE</span> puppies (</a>
<a class="sourceLine" id="cb2-3" title="3">      name <span class="dt">VARCHAR</span>(<span class="dv">50</span>),</a>
<a class="sourceLine" id="cb2-4" title="4">      age_yrs <span class="dt">NUMERIC</span>(<span class="dv">3</span>,<span class="dv">1</span>),</a>
<a class="sourceLine" id="cb2-5" title="5">      breed <span class="dt">VARCHAR</span>(<span class="dv">100</span>),</a>
<a class="sourceLine" id="cb2-6" title="6">      weight_lbs <span class="dt">INTEGER</span>,</a>
<a class="sourceLine" id="cb2-7" title="7">      microchipped <span class="dt">BOOLEAN</span></a>
<a class="sourceLine" id="cb2-8" title="8">    );</a></code></pre></div>
<p>Log into your database, if you’re not already. (Make sure you’re in <em>your</em> database by looking at the prompt. It should read <code>«your user name»=#</code>.) Type in the SQL statement from above. If you do it correctly, PostgreSQL will return the message “CREATE TABLE”.</p>
<h2 id="listing-tables-and-table-definitions">Listing tables and table definitions</h2>
<p>You can see the tables in your database by typing <code>\dt</code> at the <code>psql</code> shell prompt. The <code>\dt</code> command means “describe tables”. If you do that now, assuming that you’ve only created the “puppies” table, you should see the following with your user name, of course.</p>
<pre><code>           List of relations
 Schema |  Name   | Type  |   Owner
--------+---------+-------+------------
 public | puppies | table | appacademy</code></pre>
<p>The user that runs the SQL statement that creates the table is the owner of that table.</p>
<p>Table owners, like database owners, will always be able to access the table and its data.</p>
<p>If you want a user other than the one that you’re logged in as to own the table, you have two ways of doing that.</p>
<ul>
<li><p>1 Log out and log in as the user that you want to own the table and run the <code>CREATE TABLE</code> statement as that user.</p></li>
<li><p>2 As the superuser, run the <code>SET ROLE «user name»</code> command to switch the current user and run the <code>CREATE TABLE</code> statement as that user.</p></li>
</ul>
<p><strong>To see the definition of a particular table table, type <code>\d «table name»</code></strong>.</p>
<p>For puppies, type <code>\d puppies</code>. You should see the following output.</p>
<pre><code>                         Table &quot;public.puppies&quot;
    Column    |          Type          | Collation | Nullable | Default
--------------+------------------------+-----------+----------+---------
 name         | character varying(50)  |           |          |
 age_yrs      | numeric(3,1)           |           |          |
 breed        | character varying(100) |           |          |
 weight_lbs   | integer                |           |          |
 microchipped | boolean                |           |          |</code></pre>
<p>For now, ignore the “Collation”, “Nullable”, and “Default” columns in that output. The next article will address “Nullable” and “Default”.</p>
<p>You can see that the data types that you provided have been translated into their ANSI SQL full name equivalents.</p>
<p>Now, connect to the “postgres” database using the <code>\c postgres</code> command. It should give you a message that you’re now connected to the “postgres” database as your user. The prompt should change from one that has your name to <code>postgres=#</code>. Now, type <code>\dt</code> to list the tables in the “postgres” database. If you haven’t created any tables there, it will read “Did not find any relations.” If you type <code>\d puppies</code>, it will report that it can’t find a relation named “puppies”.</p>
<p>This is because you’re in a different database than the one in which you created the “puppies” table. You just don’t see the “puppies” table, here, because it doesn’t exit. That table is in another database, your user database. That’s how databases work: they provide an area where you can create tables in which you’ll store data. Different databases have different tables. You can’t easily get at tables in another database from the one that you’re currently in. And, really, you don’t want to. Databases provide the storage and security boundaries for data.</p>
<p><strong>Change back to your user database by executing the command <code>\c «your user name»</code></strong>.</p>
<h2 id="deleting-a-table"><strong>Deleting a table</strong></h2>
<hr />
<p>In the same way that you can delete users and databases by using the <code>DROP</code> command, you can do the same for tables.</p>
<p>To get rid of the “puppies” table, execute the following SQL command.</p>
<pre><code>DROP TABLE puppies;</code></pre>
<p>It should tell you that it dropped the table. You can confirm that it is no longer there by executing the <code>\dt</code> command.</p>
<h2 id="what-youve-done">What you’ve done</h2>
<p>In this section, you learned the basics about creating database entities called “tables” and their ownership. You learned that tables are where you store data. You discovered that the data that you store is defined by the columns and their data types. You can now write SQL to create and drop tables.</p>
<p>Next up, you will learn about special kinds of columns, column constraints, and building relations between tables.</p>
<p><a href="https://open.appacademy.io/learn/js-py---sep-2020-online/week-10-sep-2020-online/table-management-walk-through-part-1">Source</a></p>
