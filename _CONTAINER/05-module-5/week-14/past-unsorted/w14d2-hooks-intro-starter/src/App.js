import UseState from './components/UseState';
// import UseEffect from './components/UseEffect';
// import Cleanup from './components/Cleanup';
// import IssueForm from './components/IssueForm';

function App() {
  return (
    <>
      <UseState />
      {/* <UseEffect /> */}
      {/* <Cleanup /> */}
      {/* <IssueForm /> */}
    </>
  );
}

export default App;
